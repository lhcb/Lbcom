/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// This File contains the implementation of the MCParticle2MCHitAlg interface
//
// C++ code for 'LHCb Tracking package(s)'
//
//   Authors: Rutger van der Eijk
//   Created: 3-7-2002
/** @class TrackerMCParticle2MCHitAlg TrackerMCParticle2MCHitAlg.h
 *
 *   @author: M. Needham
 *   @date 03-07-2002
 */

#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Linker/LinkedFrom.h"
#include <string>
#include <vector>

class TrackerMCParticle2MCHitAlg : public GaudiAlgorithm {

public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private:
  Gaudi::Property<std::vector<std::string>> m_dets{ this, "Detectors", { "Velo", "IT", "TT", "OT", "Muon" } };
  Gaudi::Property<std::string>              m_outputData{
      this, "OutputData", "/Event/MC/Particles2MCTrackerHits" }; ///< location of Particles to associate
};

DECLARE_COMPONENT( TrackerMCParticle2MCHitAlg )

using namespace LHCb;

StatusCode TrackerMCParticle2MCHitAlg::execute() {
  // get the MCParticles
  MCParticles* particles = get<MCParticles>( MCParticleLocation::Default );

  // make the output table
  auto myLink = new LHCb::LinksByKey{ std::in_place_type<MCHit>, std::in_place_type<MCParticle>,
                                      LHCb::LinksByKey::Order::decreasingWeight };
  put( myLink, LHCb::LinksByKey::linkerName( m_outputData ) );

  // get the sub-detector linkers
  std::vector<LHCb::LinksByKey const*> links;
  links.reserve( m_dets.size() );
  std::transform( m_dets.begin(), m_dets.end(), std::back_inserter( links ), [&]( const auto& det ) {
    return get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( MCParticleLocation::Default + "2MC" + det + "Hits" ) );
  } );

  // loop over MCParticles
  for ( const auto& p : *particles ) {
    for ( auto& l : links ) {
      for ( auto const& aHit : LinkedFrom<LHCb::MCHit>{ l }.range( p ) ) { myLink->link( &aHit, p ); }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Linker table stored at " << m_outputData << endmsg;

  return StatusCode::SUCCESS;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//
// This File contains the implementation of the MCParticle2MCHitAlg interface
//
// C++ code for 'LHCb Tracking package(s)'
//
//   Authors: Rutger van der Eijk
//   Created: 3-7-2002

#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "LHCbAlgs/Transformer.h"

class MCParticle2MCHitAlg
    : public LHCb::Algorithm::Transformer<LHCb::LinksByKey( LHCb::MCHits const&, LHCb::MCParticles const& )> {

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_nullptr{ this, "Container of  MCHit contains nullptr" };

public:
  MCParticle2MCHitAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue( "MCHitPath", "" ), KeyValue( "MCParticles", LHCb::MCParticleLocation::Default ) },
                     KeyValue( "OutputData", "" ) ) {}
  LHCb::LinksByKey operator()( LHCb::MCHits const& mc_hits, LHCb::MCParticles const& ) const override {
    LHCb::LinksByKey new_links{ std::in_place_type<LHCb::MCParticle>, std::in_place_type<LHCb::MCHit> };
    for ( auto const* mc_hit : mc_hits ) {
      if ( !mc_hit ) {
        ++m_nullptr;
        continue;
      }
      new_links.link( mc_hit->mcParticle(), mc_hit );
    }
    return new_links;
  }
};

DECLARE_COMPONENT( MCParticle2MCHitAlg )

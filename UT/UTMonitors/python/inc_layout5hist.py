###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import CreateHistDBPages
from Gaudi.Configuration import *

createHistAlg = CreateHistDBPages("createHistAlg")

createHistAlg.PageLayout = []
createHistAlg.PageLayout.append([0.01, 0.676667, 0.323333, 0.99])
createHistAlg.PageLayout.append([0.343333, 0.676667, 0.656667, 0.99])
createHistAlg.PageLayout.append([0.676667, 0.676667, 0.99, 0.99])
createHistAlg.PageLayout.append([0.01, 0.343333, 0.323333, 0.656667])
createHistAlg.PageLayout.append([0.343333, 0.343333, 0.656667, 0.656667])

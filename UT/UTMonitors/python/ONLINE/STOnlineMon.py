from __future__ import print_function

from Configurables import GaudiSequencer

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *

print("---------> STOnlineMon")

from os import environ

# print 'home', environ['HOME']
try:
    task = environ["TASKNAME"]
    print("TASKNAME:", task)
except:
    print("TASKNAME is not defined")

if task in ["TTDAQMon", "TTNZSMon", "TTSumMon", "TTTAEMon"]:
    detType = "TT"
elif task in ["ITDAQMon", "ITNZSMon", "ITSumMon", "ITTAEMon"]:
    detType = "IT"
elif task in ["STCalibMon"]:
    detType = "ST"
else:
    print("***************** WTF???", task, " is unknown")

# ================================================================================

appMgr = ApplicationMgr()

import General

General.DatabaseTags(task)
General.SetupApplicationMgr(task)

from Configurables import UpdateAndReset

appMgr.TopAlg.append(UpdateAndReset())

UpdateAndReset().saveHistograms = 1
UpdateAndReset().saverCycle = 600
UpdateAndReset().resetHistosAfterSave = 1
UpdateAndReset().desiredDeltaTCycle = 5

MoniSeq = GaudiSequencer("MoniTTSeq".replace("TT", detType))
MoniSeq.MeasureTime = True
appMgr.TopAlg.append(MoniSeq)

if task == "TTDAQMon" or task == "ITDAQMon":
    from Configurables import RawBankToSTClusterAlg, ST__STClusterMonitor

    MoniSeq.Members = [
        RawBankToSTClusterAlg(DetType=detType, OutputLevel=4),
        ST__STClusterMonitor(detType + "ClusterMonitorCentral", DetType=detType),
    ]
    clusterMonitor = ST__STClusterMonitor(detType + "ClusterMonitorCentral")
    clusterMonitor.HitMaps = True
    clusterMonitor.ByDetectorRegion = False
    clusterMonitor.ByServiceBox = True
    clusterMonitor.ByPort = True
    clusterMonitor.FullDetail = False
    clusterMonitor.OutputLevel = 3
    clusterMonitor.MinTotalClusters = 0
    clusterMonitor.ChargeCut = 0
elif task == "TTNZSMon" or task == "ITNZSMon":
    importOptions(
        "$STTELL1ALGORITHMSROOT/options/TTEmulator.py".replace(
            "TTEmulator", detType + "Emulator"
        )
    )
    importOptions(
        "$STMONITORSROOT/options/TTNZSOnlineMon.py".replace("TTNZS", detType + "NZS")
    )
    # from Configurable import STTELL1CheckNzs
    MoniSeq.Members = [
        GaudiSequencer("EmuTTSeq".replace("EmuTT", "Emu" + detType)),
        GaudiSequencer("MoniTTNZSSeq".replace("MoniTT", "Moni" + detType)),
    ]
elif task == "TTTAEMon" or task == "ITTAEMon":
    importOptions("$STDAQROOT/options/TTDecodeTAE.py".replace("TT", detType))
    importOptions(
        "$STMONITORSROOT/options/TTTAEClusterMonitor.opts".replace("TT", detType)
    )
    importOptions(
        "$STMONITORSROOT/options/TTClusterMonitor.opts".replace("TT", detType)
    )
    members = MoniSeq.Members
    MoniSeq.Members = [
        GaudiSequencer("DecodeTTSeq".replace("DecodeTT", "Decode" + detType))
    ]
    MoniSeq.Members += members
elif task == "TTSumMon" or task == "ITSumMon":
    from Configurables import (
        RawBankToSTClusterAlg,
        RawBankToSTLiteClusterAlg,
        ST__STDataSizeMonitor,
        STErrorDecoding,
        STErrorMonitor,
        STSummaryMonitor,
    )

    MoniSeq.Members = [
        RawBankToSTClusterAlg(DetType=detType, OutputLevel=4),
        RawBankToSTLiteClusterAlg(DetType=detType, OutputLevel=4),
        STSummaryMonitor(DetType=detType, OutputLevel=4),
        STErrorDecoding(DetType=detType, OutputLevel=4),
        STErrorMonitor(DetType=detType, OutputLevel=4, ExpertHisto=True),
        ST__STDataSizeMonitor(detType + "DataSizeMonitor", DetType=detType),
    ]
elif task == "STCalibMon":
    importOptions("$STONMONROOT/options/CalibSTNZSMon.opts")
    pass
else:
    print("******: invalid task dickhead : ", task)

print("*********************************************")
print("TopAlgs:", appMgr.TopAlg)
print("MoniSeq:", MoniSeq.Members)
print("*********************************************")


# --- startup for online
def run():
    print("-------> run")
    import OnlineEnv as Online

    montype = "online"
    if montype == "online":
        General.patchMessages()
    # print 'shitbag'
    Online.end_config(False)
    # print "<<<<<<<<< end run"


# shite()

# print '<<<<<<<<<<<<< end STOnlineMon'

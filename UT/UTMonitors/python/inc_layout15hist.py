###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import CreateHistDBPages
from Gaudi.Configuration import *

createHistAlg = CreateHistDBPages("createHistAlg")

createHistAlg.PageLayout = []
createHistAlg.PageLayout.append([0.01, 0.76, 0.24, 0.99])
createHistAlg.PageLayout.append([0.26, 0.76, 0.49, 0.99])
createHistAlg.PageLayout.append([0.51, 0.76, 0.74, 0.99])
createHistAlg.PageLayout.append([0.76, 0.76, 0.99, 0.99])
createHistAlg.PageLayout.append([0.01, 0.51, 0.24, 0.74])
createHistAlg.PageLayout.append([0.26, 0.51, 0.49, 0.74])
createHistAlg.PageLayout.append([0.51, 0.51, 0.74, 0.74])
createHistAlg.PageLayout.append([0.76, 0.51, 0.99, 0.74])
createHistAlg.PageLayout.append([0.01, 0.26, 0.24, 0.49])
createHistAlg.PageLayout.append([0.26, 0.26, 0.49, 0.49])
createHistAlg.PageLayout.append([0.51, 0.26, 0.74, 0.49])
createHistAlg.PageLayout.append([0.76, 0.26, 0.99, 0.49])
createHistAlg.PageLayout.append([0.01, 0.01, 0.24, 0.24])
createHistAlg.PageLayout.append([0.26, 0.01, 0.49, 0.24])
createHistAlg.PageLayout.append([0.51, 0.01, 0.74, 0.24])

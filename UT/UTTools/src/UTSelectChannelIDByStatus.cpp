/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "UTDet/DeUTSector.h"

#include "Gaudi/Parsers/CommonParsers.h"
#include "GaudiAlg/GaudiTool.h"

#include <string>
#include <vector>

/**
 *  Tool for selecting clusters using the conditions
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

#ifdef USE_DD4HEP
using UTStatus = LHCb::Detector::UT::Status;
#else
using UTStatus = DeUTSector::Status;
#endif

namespace Gaudi::Parsers {

#ifdef USE_DD4HEP
  /// allow for Status and vector<Status> to be used as a Gaudi::Property
  StatusCode parse( LHCb::Detector::UT::Status& s, std::string const& str ) {
    std::string value; // get rid of any enclosing ' and " by explicitly parsing str into an std::string
    return Gaudi::Parsers::parse( value, str ).andThen( [&]() -> StatusCode {
      s = LHCb::Detector::UT::toStatus( value );
      return s == LHCb::Detector::UT::Status::UnknownStatus ? StatusCode::FAILURE : StatusCode::SUCCESS;
    } );
  }
#endif

  StatusCode parse( std::vector<UTStatus>& vec, std::string const& s ) {
    std::vector<std::string> vs;
    return Gaudi::Parsers::parse( vs, s ).andThen( [&]() -> StatusCode {
      try {
        vec.clear();
        std::transform( vs.begin(), vs.end(), std::back_inserter( vec ), []( std::string const& i ) {
          UTStatus v{};
          parse( v, i ).orThrow();
          return v;
        } );
        return StatusCode::SUCCESS;
      } catch ( StatusCode sc ) {
        vec.clear();
        return sc;
      }
    } );
  }

} // namespace Gaudi::Parsers

class UTSelectChannelIDByStatus final : public extends<GaudiTool, IUTChannelIDSelector> {

public:
  using extends::extends;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const override {
    return ( *this )( id, det );
  }

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const override {
#ifdef USE_DD4HEP
    const auto status = det.findSector( id ).stripStatus( id );
#else
    const auto status = det.findSector( id )->stripStatus( id );
#endif
    return std::find( m_statusList.begin(), m_statusList.end(), status ) != m_statusList.end();
  }

private:
  Gaudi::Property<std::vector<UTStatus>> m_statusList{ this, "allowedStatus", { UTStatus::OK } };
};

DECLARE_COMPONENT( UTSelectChannelIDByStatus )

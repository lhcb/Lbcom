/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IUTClusterSelector.h"
#include <string>

/**
 *  Helper concrete tool for selection of UTCluster objects
 *  This selector selects the cluster if
 *  at least one  of its daughter selector select it!
 *
 *  @author A Beiter (based on code by M Needham)
 *  @date   2018-09-04
 */
class UTClusterSelectorOR : public extends<GaudiTool, IUTClusterSelector> {
public:
  /// container of types&names
  using Names = std::vector<std::string>;
  /// container of selectors
  using Selectors = std::vector<IUTClusterSelector*>;

public:
  using extends::extends;

  /** "select"/"preselect" method
   *  @see IUTClusterSelector
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::UTCluster const& cluster, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see IUTClusterSelector
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::UTCluster const& cluster, DeUTDetector const& ) const override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

private:
  Gaudi::Property<Names> m_selectorsTypeNames{ this, "SelectorTools", {} };
  Selectors              m_selectors;

  // errors
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_NoSelectorSpecified{ this, "No selectors specified" };
};

DECLARE_COMPONENT( UTClusterSelectorOR )

/** stORard initialization of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
StatusCode UTClusterSelectorOR::initialize() {
  if ( m_selectorsTypeNames.empty() ) ++m_NoSelectorSpecified;
  // initialize the base class
  return GaudiTool::initialize().andThen( [&] {
    std::transform( m_selectorsTypeNames.begin(), m_selectorsTypeNames.end(), std::back_inserter( m_selectors ),
                    [&]( const auto& s ) {
                      info() << " Adding selector named " << s << endmsg;
                      return tool<IUTClusterSelector>( s );
                    } );
  } );
}

/** "select"/"preselect" method
 *  @see IUTClusterSelector
 *  @param  cluster pointer to UT cluster object to be selected
 *  @return true if cluster is selected
 */
bool UTClusterSelectorOR::select( LHCb::UTCluster const& cluster, DeUTDetector const& det ) const {
  return ( *this )( cluster, det );
}

/** "select"/"preselect" method (functor interface)
 *  @see IUTClusterSelector
 *  @param  cluster pointer to UT cluster object to be selected
 *  @return true if cluster is selected
 */
bool UTClusterSelectorOR::operator()( LHCb::UTCluster const& cluster, DeUTDetector const& det ) const {
  return !m_selectors.empty() &&
         std::any_of( m_selectors.begin(), m_selectors.end(), [&]( const auto& s ) { return ( *s )( cluster, det ); } );
}

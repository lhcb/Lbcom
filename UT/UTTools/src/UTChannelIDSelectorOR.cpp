/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IUTChannelIDSelector.h"
#include <string>

/**
 *  Helper concrete tool for selection of UTChannelID objects
 *  This selector selects the cluster if
 *  at least one  of its daughter selector select it!
 *
 *  @author A Beiter (based on code by M Needham)
 *  @date   2018-09-04
 */
class UTChannelIDSelectorOR : public extends<GaudiTool, IUTChannelIDSelector> {
public:
  /// container of types&names
  using Names = std::vector<std::string>;
  /// container of selectors
  using Selectors = std::vector<IUTChannelIDSelector*>;

public:
  /** Standard constructor
   */
  using extends::extends;

  /** "select"/"preselect" method
   *  @see IUTChannelIDSelector
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see IUTChannelIDSelector
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

private:
  Gaudi::Property<Names> m_selectorsTypeNames{ this, "SelectorTools", {} };
  Selectors              m_selectors;
};

DECLARE_COMPONENT( UTChannelIDSelectorOR )

/** stORard initialization of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
StatusCode UTChannelIDSelectorOR::initialize() {
  // initialize the base class
  return extends::initialize().andThen( [&] {
    for ( const auto& it : m_selectorsTypeNames ) m_selectors.push_back( tool<IUTChannelIDSelector>( it ) );
  } );
}

/** "select"/"preselect" method
 *  @see IUTChannelIDSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
bool UTChannelIDSelectorOR::select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  return ( *this )( id, det );
}

/** "select"/"preselect" method (functor interface)
 *  @see IUTChannelIDSelector
 *  @param  cluster pointer to calo cluster object to be selected
 *  @return true if cluster is selected
 */
bool UTChannelIDSelectorOR::operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  return std::any_of( m_selectors.begin(), m_selectors.end(),
                      [&]( const auto& selector ) { return ( *selector )( id, det ); } );
}

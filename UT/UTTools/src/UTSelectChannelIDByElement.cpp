/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/UTNames.h"

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "LHCbAlgs/Traits.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTLayer.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSide.h"

#include "GaudiAlg/FunctionalTool.h"
#include "GaudiKernel/IBinder.h"

#include <variant>

namespace {
  // generic handler for Station or Layer ot Sector
#ifdef USE_DD4HEP
  using UTElement = std::variant<DeUTSide, DeUTLayer, DeUTSector>;
#else
  using UTElement = std::variant<DeUTSide const*, DeUTLayer const*, DeUTSector const*>;
#endif

  /**
   * little struct holding derived condition, actually a vector
   * of Sector/Layer/Station to be accepted by the selection
   */
  using Elements = std::vector<UTElement>;

  /// little visitor calling contains on a UTElement
  struct Contains {
    LHCb::Detector::UT::ChannelID id;
#ifdef USE_DD4HEP
    bool operator()( DeUTSide const s ) { return s.contains( id ); }
    bool operator()( DeUTLayer const l ) { return l.contains( id ); }
    bool operator()( DeUTSector const s ) { return s.contains( id ); }
#else
    bool operator()( DeUTSide const* s ) { return s->contains( id ); }
    bool operator()( DeUTLayer const* l ) { return l->contains( id ); }
    bool operator()( DeUTSector const* s ) { return s->contains( id ); }
#endif
  };

  /// method deriving Elements from DeUTDetector
  Elements listElements( std::vector<std::string> const& names, DeUTDetector const& det ) {
    Elements elements;
    for ( const auto& name : names ) {
      if ( name.find( "UT" ) != std::string_view::npos ) {
        const LHCb::Detector::UT::ChannelID chan = LHCb::UTNames::stringToChannel( name );
#ifdef USE_DD4HEP
        if ( chan.sector() != 0 ) {
          auto s = det.findSector( chan );
          if ( s.isValid() ) {
            elements.emplace_back( s );
#else
        {
          auto s = det.findSector( chan );
          if ( s ) {
            elements.emplace_back( s );
#endif
            continue;
          }
        }
#ifdef USE_DD4HEP
        if ( chan.layer() != 0 ) {
          auto l = det.findLayer( chan );
          if ( l.isValid() ) {
            elements.emplace_back( l );
#else
        {
          auto l = det.findLayer( chan );
          if ( l ) {
            elements.emplace_back( l );
#endif
            continue;
          }
        }
#ifdef USE_DD4HEP
        if ( chan.station() != 0u ) {
          auto s = det.findStation( chan );
          if ( s.isValid() ) {
            elements.emplace_back( s );
#else
        {
          auto s = det.findSide( chan );
          if ( s ) {
            elements.emplace_back( s );
#endif
            continue;
          }
        }
      }
      throw GaudiException( "Failed to find detector element", "listElements", StatusCode::FAILURE );
    } // for each
    return elements;
  }
} // namespace

/**
 *  Tool for selecting clusters using the conditions
 *
 *  Note the usgae of the ToolBinder to hide the derivation of Elements
 *  from DeUTDetector from the interface used by clients while keeping proper
 *  derivation via standard tools.
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */
class UTSelectChannelIDByElement
    : public Gaudi::Functional::ToolBinder<Gaudi::Interface::Bind::Box<IUTChannelIDSelector>( Elements const& ),
                                           LHCb::Algorithm::Traits::usesBaseAndConditions<
                                               LHCb::DetDesc::ConditionAccessorHolder<GaudiTool>, DeUTDetector>> {

  class BoundInstance final : public Gaudi::Interface::Bind::Stub<IUTChannelIDSelector> {
    const UTSelectChannelIDByElement* m_parent;
    const Elements&                   m_elements;

  public:
    BoundInstance( UTSelectChannelIDByElement const* parent, Elements const& elems )
        : m_parent{ parent }, m_elements{ elems } {}
    bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const override {
      return m_parent->select( id, det, m_elements );
    }
    bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const override {
      return m_parent->operator()( id, det, m_elements );
    }
  };

public:
  UTSelectChannelIDByElement( std::string type, std::string name, const IInterface* parent )
      : ToolBinder{ std::move( type ),
                    std::move( name ),
                    parent,
                    { KeyValue{ "Elements", "UTSelectChannelIDByElementElements" } },
                    construct<BoundInstance>( this ) } {}

  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const&, Elements const& ) const;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const&, Elements const& ) const;

private:
  Gaudi::Property<std::vector<std::string>> m_elementNames{ this, "elementNames", {} };
};

DECLARE_COMPONENT( UTSelectChannelIDByElement )

StatusCode UTSelectChannelIDByElement::initialize() {
  return ToolBinder::initialize().andThen( [&] {
    addConditionDerivation( { DeUTDetLocation::location() }, inputLocation<Elements>(),
                            [this]( DeUTDetector const& det ) { return listElements( m_elementNames.value(), det ); } );
    return StatusCode::SUCCESS;
  } );
}

bool UTSelectChannelIDByElement::select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det,
                                         Elements const& elems ) const {
  return ( *this )( id, det, elems );
}

bool UTSelectChannelIDByElement::operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const&,
                                             Elements const&                      elems ) const {
  auto iterElem = std::find_if( begin( elems ), end( elems ),
                                [&]( UTElement const& elem ) { return std::visit( Contains{ id }, elem ); } );
  return iterElem != elems.end();
}

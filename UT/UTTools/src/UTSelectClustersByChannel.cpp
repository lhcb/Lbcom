/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Event/UTCluster.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Kernel/IUTClusterSelector.h"

#include "GaudiAlg/GaudiTool.h"

/** @class UTSelectClustersByChannel UTSelectClustersByChannel.h
 *
 *  Tool for selecting clusters using a list of service boxes
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectClustersByChannel : public extends<GaudiTool, IUTClusterSelector> {

public:
  using extends::extends;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::UTCluster const& cluster, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::UTCluster const& cluster, DeUTDetector const& ) const override;

private:
  ToolHandle<IUTChannelIDSelector> m_selector{ this, "SelectorType", "UTRndmEffSelector" };
};

DECLARE_COMPONENT( UTSelectClustersByChannel )

bool UTSelectClustersByChannel::select( LHCb::UTCluster const& cluster, DeUTDetector const& det ) const {
  return ( *this )( cluster, det );
}

bool UTSelectClustersByChannel::operator()( LHCb::UTCluster const& cluster, DeUTDetector const& det ) const {
  // just delegate to the channel selector
  return m_selector->select( cluster.channelID(), det );
}

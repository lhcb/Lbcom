/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Event/MCParticle.h"
#include "Event/UTCluster.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "Linker/LinkedTo.h"
#include "MCInterfaces/IMCParticleSelector.h"

#include "GaudiAlg/GaudiTool.h"

#include <vector>

/**
 *  Tool for selecting clusters using a list of service boxes
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectChannelIDByTruth : public extends<GaudiTool, IUTChannelIDSelector, IIncidentListener> {

public:
  using extends::extends;
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  enum Types { Spillover = 0, Real = 1 };

  mutable LHCb::LinksByKey const* m_links =
      nullptr; // FIXME: get rid of 'handle' and cached pointer to event store object
  Gaudi::Property<std::string>      m_clusterLocation{ this, "InputData", LHCb::UTClusterLocation::UTClusters };
  Gaudi::Property<std::vector<int>> m_types{ this, "Types", { Spillover, Real } };
  ToolHandle<IMCParticleSelector>   m_selector{ this, "Selector", "MCParticleSelector/Selector" };
};

DECLARE_COMPONENT( UTSelectChannelIDByTruth )

StatusCode UTSelectChannelIDByTruth::initialize() {
  return extends::initialize().andThen( [&] { incSvc()->addListener( this, IncidentType::BeginEvent ); } );
}

void UTSelectChannelIDByTruth::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) { m_links = nullptr; }
}

bool UTSelectChannelIDByTruth::select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  return ( *this )( id, det );
}

bool UTSelectChannelIDByTruth::operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const {
  // get the linker table
  if ( !m_links ) { m_links = get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( m_clusterLocation ) ); }

  // spillover or not
  int                     clusType  = Real;
  const LHCb::MCParticle* aParticle = LinkedTo<LHCb::MCParticle>( m_links ).range( id ).try_front();
  if ( aParticle ) { clusType = Spillover; }

  // check we want this type
  if ( std::find( m_types.begin(), m_types.end(), clusType ) == m_types.end() ) { return false; }

  // select using the select [if there is a link]
  return !aParticle || m_selector->accept( aParticle );
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IUTClusterSelector.h"
#include <string>

/**
 *  Helper concrete tool for selection of stcluster objects
 *  This selector selects the cluster if
 *  all of its daughter selector select it!
 *
 *  @author A Beiter (based on code by M Needhams)
 *  @date   2018-09-04
 */
class UTClusterSelectorAND : public extends<GaudiTool, IUTClusterSelector> {
public:
  /// container of types&names
  using Names = std::vector<std::string>;
  /// container of selectors
  using Selectors = std::vector<IUTClusterSelector*>;

public:
  using extends::extends;

  /** "select"/"preselect" method
   *  @see IUTClusterSelector
   *  @param  cluster pointer to st cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::UTCluster const& cluster, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @see ICaloClusterSelector
   *  @param  cluster pointer to st cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::UTCluster const& cluster, DeUTDetector const& ) const override;

  /** standard initialization of the tool
   *  @see IAlgTool
   *  @see AlgTool
   *  @see GaudiTool
   *  @return status code
   */
  StatusCode initialize() override;

private:
  Gaudi::Property<Names> m_selectorsTypeNames{ this, "SelectorTools", {} };
  Selectors              m_selectors;
};

DECLARE_COMPONENT( UTClusterSelectorAND )

/** standard initialization of the tool
 *  @see IAlgTool
 *  @see AlgTool
 *  @see GaudiTool
 *  @return status code
 */
StatusCode UTClusterSelectorAND::initialize() {
  return GaudiTool::initialize().andThen( [&] {
    for ( const auto& tn : m_selectorsTypeNames ) m_selectors.push_back( tool<IUTClusterSelector>( tn ) );
  } );
}

/** "select"/"preselect" method
 *  @see IUTClusterSelector
 *  @param  cluster pointer to UT cluster object to be selected
 *  @return true if cluster is selected
 */
bool UTClusterSelectorAND::select( LHCb::UTCluster const& cluster, DeUTDetector const& det ) const {
  return ( *this )( cluster, det );
}

/** "select"/"preselect" method (functor interface)
 *  @see IUTClusterSelector
 *  @param  cluster pointer to UT cluster object to be selected
 *  @return true if cluster is selected
 */
bool UTClusterSelectorAND::operator()( LHCb::UTCluster const& cluster, DeUTDetector const& det ) const {

  return !m_selectors.empty() &&
         std::all_of( m_selectors.begin(), m_selectors.end(),
                      [&cluster, &det]( const auto& selector ) { return ( *selector )( cluster, det ); } );
}

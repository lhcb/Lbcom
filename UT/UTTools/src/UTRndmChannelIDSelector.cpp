/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"
#include "Kernel/IUTChannelIDSelector.h"

#include "GaudiAlg/GaudiTool.h"

/**
 *  Tool for selecting clusters at random
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */
class UTRndmChannelIDSelector : public extends<GaudiTool, IUTChannelIDSelector> {

public:
  using extends::extends;
  StatusCode initialize() override;

  /**  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

private:
  // smart interface to generator
  SmartIF<IRndmGen> m_uniformDist;

  Gaudi::Property<double> m_fractionToReject{ this, "FractionToReject", 0.0 };

  // errors
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_GeneratorInitFailure{ this, "Failed to init generator" };
};

DECLARE_COMPONENT( UTRndmChannelIDSelector )

StatusCode UTRndmChannelIDSelector::initialize() {
  return extends::initialize().andThen( [&]() -> StatusCode {
    /// initialize, flat generator...
    auto tRandNumSvc = service<IRndmGenSvc>( "RndmGenSvc", true );
    m_uniformDist    = tRandNumSvc->generator( Rndm::Flat( 0., 1.0 ) );
    if ( !m_uniformDist ) ++m_GeneratorInitFailure;
    return StatusCode::SUCCESS;
  } );
}

bool UTRndmChannelIDSelector::select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  return ( *this )( id, det );
}

bool UTRndmChannelIDSelector::operator()( LHCb::Detector::UT::ChannelID const&, DeUTDetector const& ) const {
  return m_uniformDist->shoot() < m_fractionToReject;
}

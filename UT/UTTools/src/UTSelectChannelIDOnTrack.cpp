/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Event/Track.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "Kernel/IUTChannelIDSelector.h"

#include "GaudiAlg/GaudiTool.h"

using namespace LHCb;
using namespace Gaudi;

/**
 *  Tool for selecting clusters that are not spillover
 *  Requires you have the previous spill, ie TAE or upgrade !
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectChannelIDOnTrack : public extends<GaudiTool, IUTChannelIDSelector, IIncidentListener> {

public:
  using extends::extends;
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

private:
  void initEvent() const;

  DataObjectReadHandle<LHCb::Tracks> m_trackLocation{ this, "trackLocation", LHCb::TrackLocation::Default };
  mutable std::vector<LHCb::Detector::UT::ChannelID> m_ids;
  mutable bool                                       m_configured = false;
};

DECLARE_COMPONENT( UTSelectChannelIDOnTrack )

StatusCode UTSelectChannelIDOnTrack::initialize() {
  return extends::initialize().andThen( [&] { incSvc()->addListener( this, IncidentType::BeginEvent ); } );
}

void UTSelectChannelIDOnTrack::handle( const Incident& incident ) {
  if ( IncidentType::BeginEvent == incident.type() ) { m_configured = false; }
}

bool UTSelectChannelIDOnTrack::select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  return ( *this )( id, det );
}

bool UTSelectChannelIDOnTrack::operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const {
  if ( !m_configured ) {
    // get the spillover clusters
    initEvent();
    m_configured = true;
  }
  return std::binary_search( m_ids.begin(), m_ids.end(), id );
}

void UTSelectChannelIDOnTrack::initEvent() const {
  m_ids.clear();
  for ( const auto& t : *m_trackLocation.get() ) {
    for ( LHCb::LHCbID chan : t->lhcbIDs() ) {
      if ( chan.isUT() ) { m_ids.push_back( chan.utID() ); }
    }
  }
  std::sort( m_ids.begin(), m_ids.end() );
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "UTDet/DeUTDetector.h"

#include "GaudiAlg/GaudiTool.h"

/**
 *  Tool for selecting clusters using the conditions
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectLadders17 : public extends<GaudiTool, IUTChannelIDSelector> {

public:
  /// constructer
  using extends::extends;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;
};

DECLARE_COMPONENT( UTSelectLadders17 )

bool UTSelectLadders17::select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  return ( *this )( id, det );
}

bool UTSelectLadders17::operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const {
  unsigned int sector = id.sector();
  return sector == 1 || sector == 7;
}

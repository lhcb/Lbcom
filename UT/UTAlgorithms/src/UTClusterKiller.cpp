/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/UT/ChannelID.h"
#include "Event/UTCluster.h"
#include "Kernel/IUTClusterSelector.h"
#include "UTDet/DeUTDetector.h"

#include "DetDesc/GenericConditionAccessorHolder.h"

#include "Gaudi/Accumulators.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/DataObjectHandle.h"

/**
 *  Class for killing clusters
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */
class UTClusterKiller : public LHCb::DetDesc::ConditionAccessorHolder<Gaudi::Algorithm> {

public:
  using ConditionAccessorHolder::ConditionAccessorHolder;
  StatusCode execute( const EventContext& ) const override;
  StatusCode finalize() override;

private:
  mutable Gaudi::Accumulators::BinomialCounter<> m_counter{ this, "fraction of clusters killed" };

  PublicToolHandle<IUTClusterSelector>   m_clusterSelector{ this, "Selector", "UTSelectClustersByChannel/UTKiller" };
  DataObjectReadHandle<LHCb::UTClusters> m_clusters{ this, "InputLocation", LHCb::UTClusterLocation::UTClusters };

  // FIXME One should not use ConditionAccessors by hand in a Tool
  // The Conditions should be declared at the algorithm level
  // and be used transparently via the functional framework
  LHCb::DetDesc::ConditionAccessor<DeUTDetector> m_detector{ this, "DeUTLocation", DeUTDetLocation::location() };
};

DECLARE_COMPONENT( UTClusterKiller )

StatusCode UTClusterKiller::execute( const EventContext& evtCtx ) const {
#ifdef USE_DD4HEP
  auto
#else
  auto&
#endif
                    det         = m_detector.get( getConditionContext( evtCtx ) );
  LHCb::UTClusters* clusterCont = m_clusters.get();
  // make list of clusters to remove
  std::vector<LHCb::Detector::UT::ChannelID> chanList;
  chanList.reserve( 100 );
  auto buffer = m_counter.buffer();
  // select the cluster to remove
  for ( const auto& iterC : *clusterCont ) {
    const bool select = ( *m_clusterSelector )( *iterC, det );
    buffer += select;
    if ( select ) chanList.push_back( iterC->key() );
  }
  // remove from the container
  for ( auto iterVec = chanList.rbegin(); iterVec != chanList.rend(); ++iterVec ) {
    clusterCont->erase( *iterVec );
  } // iterVec
  return StatusCode::SUCCESS;
}

StatusCode UTClusterKiller::finalize() {
  info() << "Fraction of clusters killed " << 100 * m_counter.efficiency() << " %" << endmsg;
  return Algorithm::finalize();
}

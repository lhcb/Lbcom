/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/UT/ChannelID.h"
#include "Event/UTLiteCluster.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "UTDet/DeUTDetector.h"

#include "DetDesc/GenericConditionAccessorHolder.h"

#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/DataObjectHandle.h"

using namespace LHCb;

/**
 *  Class for killing clusters and random
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTLiteClusterKiller : public LHCb::DetDesc::ConditionAccessorHolder<Gaudi::Algorithm> {

public:
  using ConditionAccessorHolder::ConditionAccessorHolder;

  StatusCode execute( const EventContext& evtCtx ) const override {
#ifdef USE_DD4HEP
    auto
#else
    auto&
#endif
                                   det         = m_detector.get( getConditionContext( evtCtx ) );
    UTLiteCluster::UTLiteClusters* clusterCont = m_clusters.get();
    std::remove_if( clusterCont->begin(), clusterCont->end(),
                    [&]( const LHCb::UTLiteCluster& c ) { return ( *m_clusterSelector )( c.channelID(), det ); } );
    return StatusCode::SUCCESS;
  }

private:
  PublicToolHandle<IUTChannelIDSelector> m_clusterSelector{ this, "Selector", "UTRndmChannelIDSelector/UTLiteKiller" };
  DataObjectReadHandle<UTLiteCluster::UTLiteClusters> m_clusters{ this, "InputLocation",
                                                                  UTLiteClusterLocation::UTClusters };
  // FIXME One should not use ConditionAccessors by hand in a Tool
  // The Conditions should be declared at the algorithm level
  // and be used transparently via the functional framework
  LHCb::DetDesc::ConditionAccessor<DeUTDetector> m_detector{ this, "DeUTLocation", DeUTDetLocation::location() };
};

DECLARE_COMPONENT( UTLiteClusterKiller )

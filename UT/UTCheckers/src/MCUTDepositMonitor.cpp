/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/MCUTDeposit.h"
#include "Kernel/UTNames.h"

#include "LHCbAlgs/Consumer.h"

#include <Gaudi/Accumulators/StaticHistogram.h>

#include <mutex>

/**
 *  Make plots for MCUTDeposits
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */
class MCUTDepositMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::MCUTDeposits& )> {
public:
  MCUTDepositMonitor( const std::string& name, ISvcLocator* svcloc );
  void operator()( const LHCb::MCUTDeposits& depositsCont ) const override;

private:
  void                                            fillHistograms( const LHCb::MCUTDeposit& aDeposit ) const;
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDeposits{ this, "1", "Number of deposits", { 200, 0., 10000. } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_depositedCharge{ this, "2", "Deposited charge", { 100, 0., 100. } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDepositsPerStation{
      this, "3", "Number of deposits per station", { 5, -0.5, 4.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDepositsPerLayer{
      this, "4", "Number of deposits per layer", { 41, -0.5, 40.5 } };
  mutable std::map<std::string, Gaudi::Accumulators::StaticHistogram<2>> m_depositedChargeXvY;
  mutable std::mutex                                                     m_depositedChargeXvYLock;
};

DECLARE_COMPONENT( MCUTDepositMonitor )

MCUTDepositMonitor::MCUTDepositMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{ name, pSvcLocator, { "InputData", LHCb::MCUTDepositLocation::UTDeposits } } {}

void MCUTDepositMonitor::operator()( const LHCb::MCUTDeposits& depositsCont ) const {
  // number of digits
  ++m_nDeposits[depositsCont.size()];
  // histos per digit
  for ( const auto& i : depositsCont ) fillHistograms( *i );
}

void MCUTDepositMonitor::fillHistograms( const LHCb::MCUTDeposit& aDeposit ) const {
  // Plot deposited charge
  ++m_depositedCharge[aDeposit.depositedCharge()];

  // Plot number of deposits per station
  const int iStation = aDeposit.channelID().station();
  ++m_nDepositsPerStation[iStation];

  // by layer
  ++m_nDepositsPerLayer[10 * iStation + aDeposit.channelID().layer()];

  // detailed level histos
  const LHCb::MCHit* aHit = aDeposit.mcHit();
  if ( aHit ) {
    // take midPoint
    Gaudi::XYZPoint impactPoint = aHit->midPoint();

    // fill x vs y scatter plots
    std::string title = "x vs y " + LHCb::UTNames::StationToString( aDeposit.channelID() );
    if ( !m_depositedChargeXvY.contains( title ) ) {
      // create new histo, stay thread safe
      std::scoped_lock lock{ m_depositedChargeXvYLock };
      if ( !m_depositedChargeXvY.contains( title ) ) {
        // now we are alone and there is still nothing, let's create the histogram
        m_depositedChargeXvY.emplace( std::piecewise_construct, std::forward_as_tuple( title ),
                                      std::forward_as_tuple( this, fmt::format( "{}", 200 + iStation ), title,
                                                             Gaudi::Accumulators::Axis<double>{ 50, -1000., 1000. },
                                                             Gaudi::Accumulators::Axis<double>{ 50, -1000., 1000. } ) );
      }
    }
    ++m_depositedChargeXvY.at( title )[{ impactPoint.x(), impactPoint.y() }];
  }
}

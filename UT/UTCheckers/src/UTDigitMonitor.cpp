/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTDigit.h"
#include "Kernel/UTDigitFun.h"
#include "UTDet/DeUTDetector.h"

#include "LHCbAlgs/Consumer.h"

#include <Gaudi/Accumulators/StaticHistogram.h>

#include <mutex>

using namespace LHCb;

/** Class for monitoring UTDigits
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTDigitMonitor : public LHCb::Algorithm::Consumer<void( UTDigits const&, DeUTDetector const& ),
                                                        LHCb::Algorithm::Traits::usesConditions<DeUTDetector>> {

  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDigits{ this, "1", "Number of digits", { 500, 0., 10000. } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_shorth{ this, "shorth", "shorth", { 200, 0., 100. } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDigitsPerStation{
      this, "2", "Number of digits per station", { 5, -0.5, 4.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDigitsPerLayer{
      this, "3", "Number of digits per layer", { 41, -0.5, 40.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<2>                        m_digitsVsXY{ this,
                                                                "UTDigits X vs. Y (Strip Center)",
                                                                "UTDigits X vs. Y (Strip Center)",
                                                                                       { 57, -908.6, 908.6 },
                                                                                       { 24, -764.8, 764.8 } };
  mutable std::map<std::string, Gaudi::Accumulators::StaticHistogram<1>> m_chargePerSector;
  mutable std::mutex                                                     m_chargePerSectorLock;

public:
  UTDigitMonitor( const std::string& name, ISvcLocator* svcloc )
      : Consumer{ name,
                  svcloc,
                  { { "InputData", UTDigitLocation::UTDigits }, { "UTLocation", DeUTDetLocation::location() } } } {}

  void operator()( const UTDigits& digitsCont, DeUTDetector const& det ) const override {
    // number of digits
    ++m_nDigits[digitsCont.size()];
    // histos per digit
    for ( const auto& d : digitsCont ) fillHistograms( d, det );

    double shorth = SiChargeFun::shorth( digitsCont.begin(), digitsCont.end() );
    ++m_shorth[shorth];
  }

private:
  void fillHistograms( const LHCb::UTDigit* aDigit, DeUTDetector const& det ) const {
    // histogram by station
    const int iStation = aDigit->station();
    ++m_nDigitsPerStation[iStation];
    // by layer
    const int iLayer = aDigit->layer();
    ++m_nDigitsPerLayer[10 * iStation + iLayer];

    // digits x vs. y
    auto hitChanID = aDigit->channelID();
    auto aSector   = det.findSector( hitChanID );
#ifdef USE_DD4HEP
    if ( aSector.isValid() ) {
#else
    if ( aSector ) {
#endif
#ifdef USE_DD4HEP
      auto aStrip = aSector.createTraj( hitChanID.strip(), 0 );
#else
      auto aStrip = aSector->trajectory( hitChanID, 0 );
#endif
      ROOT::Math::XYZPoint g1     = aStrip.beginPoint();
      ROOT::Math::XYZPoint g2     = aStrip.endPoint();
      ROOT::Math::XYZPoint hitPos = g1 + ( g2 - g1 ) * 0.5;
      auto                 hitX   = hitPos.X();
      auto                 hitY   = hitPos.Y();
      ++m_digitsVsXY[{ hitX, hitY }];
    }

    auto const sector = det.findSector( aDigit->channelID() );
#ifdef USE_DD4HEP
    std::string type = sector.isValid() ? sector.type() + "/1" : "Unknown/1";
#else
    std::string type = sector ? sector->type() + "/1" : "Unknown/1";
#endif
    if ( !m_chargePerSector.contains( type ) ) {
      // create new histo, stay thread safe
      std::scoped_lock lock{ m_chargePerSectorLock };
      if ( !m_chargePerSector.contains( type ) ) {
        // now we are alone and there is still nothing, let's create the histogram
        m_chargePerSector.emplace( std::piecewise_construct, std::forward_as_tuple( type ),
                                   std::forward_as_tuple( this, "chargeDeposited_" + type,
                                                          "Deposited charge in " + type + " sectors",
                                                          Gaudi::Accumulators::Axis<double>{ 31, 0., 31. } ) );
      }
    }
    ++m_chargePerSector.at( type )[aDigit->depositedCharge()];
  }
};

DECLARE_COMPONENT( UTDigitMonitor )

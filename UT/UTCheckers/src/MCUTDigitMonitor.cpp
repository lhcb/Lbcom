/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCUTDigit.h"

#include "LHCbAlgs/Consumer.h"
#include "UTDet/DeUTDetector.h"

#include <Gaudi/Accumulators/StaticHistogram.h>

/**
 *  Make plots for MCUTDigits
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */
class MCUTDigitMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::MCUTDigits&, const DeUTDetector& ),
                                                          LHCb::Algorithm::Traits::usesConditions<DeUTDetector>> {
public:
  MCUTDigitMonitor( const std::string& name, ISvcLocator* svcloc );
  void operator()( const LHCb::MCUTDigits&, const DeUTDetector& deUT ) const override;

private:
  void fillHistograms( const LHCb::MCUTDigit& aDigit, const DeUTDetector& deUT ) const;
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nMCDigits{ this, "1", "Number of MCDigits", { 100, 0., 10000. } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDepositsPerDigit{
      this, "2", "Number of deposits per digit", { 11, -0.5, 10.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDigitsPerStation{
      this, "3", "Number of digits per station", { 11, -0.5, 4.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<2> m_digitsVsXY{ this,
                                                                "UTDigits X vs. Y (Strip Center)",
                                                                "UTDigits X vs. Y (Strip Center)",
                                                                { 57, -908.6, 908.6 },
                                                                { 24, -764.8, 764.8 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nDigitsPerLayer{
      this, "Number of digits per layer", "Number of digits per layer", { 41, -0.5, 40.5 } };
};

DECLARE_COMPONENT( MCUTDigitMonitor )

MCUTDigitMonitor::MCUTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{ name,
                pSvcLocator,
                { { "InputData", LHCb::MCUTDigitLocation::UTDigits }, { "DeUT", DeUTDetLocation::location() } } } {}

void MCUTDigitMonitor::operator()( const LHCb::MCUTDigits& digitsCont, const DeUTDetector& deUT ) const {
  // number of digits
  ++m_nMCDigits[digitsCont.size()];
  // histos per digit
  for ( const auto& i : digitsCont ) fillHistograms( *i, deUT );
}

void MCUTDigitMonitor::fillHistograms( const LHCb::MCUTDigit& aDigit, const DeUTDetector& deUT ) const {
  // number of deposits that contribute
  ++m_nDepositsPerDigit[aDigit.mcDeposit().size()];

  // histogram by station
  const int iStation = aDigit.channelID().station();
  ++m_nDigitsPerStation[iStation];

  // digits x vs. y
  auto hitChanID = aDigit.channelID();
  auto aSector   = deUT.findSector( aDigit.channelID() );
#ifdef USE_DD4HEP
  if ( aSector.isValid() ) {
#else
  if ( aSector ) {
#endif
#ifdef USE_DD4HEP
    auto aStrip = aSector.createTraj( hitChanID.strip(), 0 );
#else
    auto aStrip = aSector->trajectory( hitChanID, 0 );
#endif
    ROOT::Math::XYZPoint g1     = aStrip.beginPoint();
    ROOT::Math::XYZPoint g2     = aStrip.endPoint();
    ROOT::Math::XYZPoint hitPos = g1 + ( g2 - g1 ) * 0.5;
    auto                 hitX   = hitPos.X();
    auto                 hitY   = hitPos.Y();
    ++m_digitsVsXY[{ hitX, hitY }];
  }

  // by layer
  const int iLayer = aDigit.channelID().layer();
  ++m_nDigitsPerLayer[10 * iStation + iLayer];
}

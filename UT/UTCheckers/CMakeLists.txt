###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
UT/UTCheckers
-------------
#]=======================================================================]

gaudi_add_module(UTCheckers
    SOURCES
        src/MCUTDepositMonitor.cpp
        src/MCUTDigitMonitor.cpp
        src/UTDigitMonitor.cpp
        src/UTEffChecker.cpp
    LINK
        ROOT::Hist
        ROOT::GenVector
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        LHCb::DigiEvent
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::LHCbTrackInterfaces
        LHCb::LinkerEvent
        LHCb::MCEvent
        LHCb::MCInterfaces
        LHCb::PartPropLib
        LHCb::RecEvent
        LHCb::UTDetLib
        LHCb::UTKernelLib
)

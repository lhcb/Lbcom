/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators/Histogram.h"

namespace {
  template <typename T>
  using axis1D             = Gaudi::Accumulators::Axis<T>;
  const auto AxisBCID      = axis1D<double>( 3565, -0.5, 3564.5, "BCID" );
  const auto AxisBXType    = axis1D<double>( 4, -0.5, 3.5, "BXType", { "ee", "be", "eb", "bb" } );
  const auto AxisETOverlap = axis1D<double>( 4, -0.5, 3.5, "ET", { "0", "NoBias", "Lumi", "NoBias+Lumi" } );
  const auto AxisBit       = axis1D<double>( 25, -0.5, 24.5, "bit",
                                       { "BX&1",
                                               "BX&2",
                                               "VeloOpen",
                                               "et_bit_01",
                                               "NoBias",
                                               "Lumi",
                                               "et_bit_04",
                                               "et_bit_05",
                                               "et_bit_06",
                                               "et_bit_07",
                                               "et_bit_08 (isolated)",
                                               "et_bit_09 (leading)",
                                               "et_bit_10 (trailing)",
                                               "et_bit_11 (empty before isolated)",
                                               "et_bit_12 (empty after isolated)",
                                               "et_bit_13 (empty before leading)",
                                               "et_bit_14 (empty after trailing)",
                                               "et_bit_15 (empty isolated)",
                                               "CalibA",
                                               "CalibB",
                                               "CalibC",
                                               "CalibD",
                                               "NZS",
                                               "TAECentral",
                                               "StepRunEnable" } );
  const auto AxisTime      = Gaudi::Accumulators::Axis<double>( 7200 / 5, 0, 7200, "gpsTime / s % 7200" );
  const auto AxisScalar    = axis1D<double>( 1, -0.5, 0.5, "", { "value" } );

  using EventTypes = LHCb::ODIN::EventTypes;
} // namespace

class ODINMonitor final : public LHCb::Algorithm::Consumer<void( const LHCb::ODIN& odin )> {
public:
  ODINMonitor( const std::string& name, ISvcLocator* pSvcLocator ) : Consumer{ name, pSvcLocator, { "Input", "" } } {}

  void operator()( const LHCb::ODIN& odin ) const override;

private:
  // the next 5 plots are maintained in ODIN/... for backward compatibility
  mutable Gaudi::Accumulators::Histogram<1> m_bcid{ this, "ODIN/Bcids", "BCIDs", AxisBCID };
  mutable Gaudi::Accumulators::Histogram<1> m_bxtype{ this, "ODIN/BXType", "BXType", AxisBXType };
  mutable Gaudi::Accumulators::Histogram<1> m_trgtype{ this, "ODIN/TrgType", "TrgType", { 16, -0.5, 15.5 } };
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_bxtype{ this, "ODIN/BXTypeVsBXID", "BXTypeVsBXID", AxisBCID,
                                                           AxisBXType };
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_trgtype{ this, "ODIN/TrgTypeVsBXID", "TrgTypeVsBXID", AxisBCID,
                                                            axis1D<double>{ 16, -0.5, 15.5 } };

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_run{ this, "Run", "Run", AxisScalar };
  mutable Gaudi::Accumulators::Histogram<1>        m_run1000{
      this, "Run1000", "Run number % 1000", { 1000, -0.5, 1000 - 0.5 } };
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_tck{ this, "TCK", "TCK", AxisScalar };
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_time_step{ this, "StepVsTime", "StepVsTime", AxisTime };

  mutable Gaudi::Accumulators::Histogram<2> m_bxtype_etoverlap{ this, "ETOverlapVsBXType", "ETOverlapVsBXType",
                                                                AxisBXType, AxisETOverlap };
  mutable Gaudi::Accumulators::Histogram<1> m_calibtype{ this, "CalibType", "CalibType bitmask (A+2*B+4*C+8*D)",
                                                         axis1D<double>{ 16, -0.5, 15.5 } };
  mutable Gaudi::Accumulators::Histogram<2> m_bcid_bit{ this, "BitVsBXID", "EventType/CalibType/... bits", AxisBCID,
                                                        AxisBit };
};

DECLARE_COMPONENT( ODINMonitor )

void ODINMonitor::operator()( const LHCb::ODIN& odin ) const {
  const auto bcid    = odin.bunchId();
  const auto bx      = static_cast<uint8_t>( odin.bunchCrossingType() );
  const auto trgtype = odin.triggerType();
  const auto timeBin = ( odin.gpsTime() / 1000000 ) % 7200;

  ++m_bcid[bcid];
  ++m_bxtype[bx];
  ++m_trgtype[trgtype];
  ++m_bcid_bxtype[{ bcid, bx }];
  ++m_bcid_trgtype[{ bcid, trgtype }];

  ++m_run1000[odin.runNumber() % 1000];
  m_run[0] += odin.runNumber();
  m_tck[0] += odin.triggerConfigurationKey();
  m_time_step[timeBin] += odin.calibrationStep();
  ++m_calibtype[odin.calibrationType()];
  int eto = odin.eventTypeBit( EventTypes::NoBias ) + 2 * odin.eventTypeBit( EventTypes::Lumi );
  ++m_bxtype_etoverlap[{ bx, eto }];
  {
    unsigned int bit = 0;
    if ( bx & 1 ) ++m_bcid_bit[{ bcid, bit }];
    ++bit;
    if ( bx & 2 ) ++m_bcid_bit[{ bcid, bit }];
    ++bit;
    for ( int et = 0; et < 16; ++et, ++bit ) {
      if ( odin.eventTypeBit( static_cast<EventTypes>( 1 << et ) ) ) ++m_bcid_bit[{ bcid, bit }];
    }
    for ( int ct = 0; ct < 4; ++ct, ++bit ) {
      if ( odin.calibrationTypeBit( static_cast<LHCb::ODIN::CalibrationTypes>( 1 << ct ) ) )
        ++m_bcid_bit[{ bcid, bit }];
    }
    if ( odin.nonZeroSuppressionMode() ) ++m_bcid_bit[{ bcid, bit }];
    ++bit;
    if ( odin.timeAlignmentEventCentral() ) ++m_bcid_bit[{ bcid, bit }];
    ++bit;
    if ( odin.stepRunEnable() ) ++m_bcid_bit[{ bcid, bit }];
    ++bit;
    assert( bit == AxisBit.nBins );
  }
}

#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  Configuration file to run Calo-MC-associations on-demand
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2009-08-08
# =============================================================================
"""

Configuration file to run Calorimeter Monte Carlo Truth Associations on-demand

"""

from __future__ import print_function

# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
__version__ = "1.4$"
# =============================================================================
# nothing to be imported
__all__ = ("caloMCTruth",)
# =============================================================================
import logging

from CaloKernel.ConfUtils import getAlgo
from Configurables import CaloClusterMCTruth
from Gaudi.Configuration import *

_log = logging.getLogger("CaloAssociators")


# =============================================================================
## define configuration
def caloMCTruth(context="Offline", enableMCOnDemand=True, Digits="Ecal"):
    """
    Define the configuration for on-demand association for Calo objects
    """

    from CaloDAQ.CaloDigits import caloDigits

    ## configure 'digits'
    caloDigits("Offline", enableMCOnDemand, False, [Digits])

    from Configurables import GaudiSequencer

    main = getAlgo(
        GaudiSequencer,
        "CaloMCTruth",
        context,
        "",
        False,
    )
    main.Members = []

    clusters = getAlgo(
        CaloClusterMCTruth,
        "CaloClusterMCTruth",
        context,
        "Relations/Rec/Calo/Clusters",
        enableMCOnDemand,
    )

    main.Members += [clusters]

    return main


# =============================================================================
if "__main__" == __name__:
    print(__doc__)
    print(__author__)
    print(__version__)
    print(printOnDemand())

# =============================================================================
# The END
# =============================================================================

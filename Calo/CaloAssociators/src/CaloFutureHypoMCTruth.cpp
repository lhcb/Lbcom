/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloHypos_v2.h"
#include "Event/MCParticle.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/RelationWeighted1D.h"

/** @class Hypo2MCParticle Hypo2MCParticle.cpp
 *
 *  Simple algorithm to build Linkers for CaloHypo -> MCParticle relations
 *
 *
 *  @author Olivier Deschamps odescham@in2p3.fr
 *  @date 2012-0§-24
 */

namespace LHCb::Calo::Associators {
  using namespace LHCb::CaloFuture2MC;

  class Hypo2MCParticle
      : public LHCb::Algorithm::Transformer<HypoTable( const LHCb::Event::Calo::Hypotheses&, const ClusterTable& )> {
  public:
    Hypo2MCParticle( const std::string& name, ISvcLocator* pSvcLocator );
    HypoTable operator()( const LHCb::Event::Calo::Hypotheses&, const ClusterTable& ) const override;

  private:
    mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_links_per_hypo{ this, "#links/hypo" };
    mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_links{ this, "#links" };
  };

  DECLARE_COMPONENT_WITH_ID( Hypo2MCParticle, "CaloFutureHypoMCTruth" )

  Hypo2MCParticle::Hypo2MCParticle( const std::string& name, ISvcLocator* pSvc )
      : Transformer{ name,
                     pSvc,
                     { KeyValue{ "InputHypos", "" },
                       KeyValue{ "InputClusterTable", "Relations/" + LHCb::CaloClusterLocation::Default } },
                     KeyValue{ "OutputTable", "" } } {}

  HypoTable Hypo2MCParticle::operator()( const LHCb::Event::Calo::Hypotheses& hypos,
                                         const ClusterTable&                  cluster2MC ) const {

    HypoTable out;
    int       links = 0;
    for ( const auto& hypo : hypos.scalar() ) {
      std::map<LHCb::MCParticle const*, float> map;
      for ( const auto& cluster : hypo.clusters() ) {
        for ( const auto& ir : cluster2MC.relations( cluster.cellID() ) ) {
          auto [it, ok] = map.try_emplace( ir.to(), ir.weight() );
          if ( !ok ) it->second += ir.weight();
        }
      }
      int hlinks = 0;
      for ( const auto& [particle, weight] : map ) {
        out.i_push( hypo.cellID(), particle, weight );
        ++hlinks;
        ++links;
      }
      m_links_per_hypo += hlinks;
    }
    m_links += links;
    out.i_sort();
    return out;
  }
} // namespace LHCb::Calo::Associators

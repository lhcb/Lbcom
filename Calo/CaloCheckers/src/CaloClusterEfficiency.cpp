/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Location.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"
#include "Relations/RelationWeighted1D.h"

using Cluster2MCTable = LHCb::CaloFuture2MC::ClusterTable;
using MC2ClusterTable = LHCb::CaloFuture2MC::MC2ClusterTable;
using Digit2MCTable   = LHCb::CaloFuture2MC::DigitTable;
using MC2DigitTable   = LHCb::CaloFuture2MC::MC2DigitTable;

namespace LHCb::Calo::Algorithms {

  /**
   *  Class to check cluster reconstruction efficiency
   *  Check how many MCParticles of a certain ID, certain MCParent ID,
   *  that deposit energy in ECAL and have a minimum PT are matched to
   *  reconstructed clusters.
   *  It uses the relation table produced by CaloClusterMCTruth
   *  to match MCParticles and reconstructed CaloClusters
   *
   *  Matching criteria:
   *  - reconstructibe: at least m_minMCFraction of the MCParticle energy is deposited
   *  in the CaloDigits (default 90%) and the cross neighbouring cells from the MCParticle
   *  are isolated at least m_minIsolation (default 0%).
   *  - reconstructed: at least m_minMatchFraction of the CaloCluster energy is coming
   *  from the MCParticle (default 90%) and at least m_minMCInCluster fraction of the
   *  MCParticle energy is contained inside the reconstructed cluster (default 90%).
   *
   *  @author Carla Marin carla.marin@cern.ch based on
   *  CaloClusterResolution.cpp by Sasha Zenaiev oleksandr.zenaiev@cern.ch
   *
   *  @date   2020-04-24
   */
  class ClusterEfficiency
      : public LHCb::Algorithm::Consumer<void( const Cluster2MCTable&, const LHCb::MCParticles&,
                                               const LHCb::Event::Calo::v2::Clusters&, const LHCb::Event::Calo::Digits&,
                                               const DeCalorimeter& ),
                                         LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>> {
  public:
    using base_type =
        LHCb::Algorithm::Consumer<void( const Cluster2MCTable&, const LHCb::MCParticles&,
                                        const LHCb::Event::Calo::v2::Clusters&, const LHCb::Event::Calo::Digits&,
                                        const DeCalorimeter& ),
                                  LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>>;
    using KeyValue = typename base_type::KeyValue;

    ClusterEfficiency( const std::string& name, ISvcLocator* pSvc )
        : base_type{ name,
                     pSvc,
                     {
                         KeyValue{ "inputRelationsCluster", "Relations/" + LHCb::CaloClusterLocation::Default },
                         KeyValue{ "inputMCParticles", LHCb::MCParticleLocation::Default },
                         KeyValue{ "recoClusters", LHCb::Event::Calo::ClusterLocation::Default },
                         KeyValue{ "caloDigits", LHCb::CaloDigitLocation::Ecal },
                         KeyValue{ "Detector", DeCalorimeterLocation::Ecal },
                     } } {}

    void operator()( const Cluster2MCTable& table, const LHCb::MCParticles& mcparts,
                     const LHCb::Event::Calo::v2::Clusters& clusters, const LHCb::Event::Calo::Digits& digits,
                     const DeCalorimeter& calo ) const override {

      const auto cluster_index = clusters.index();
      if ( !cluster_index ) {
        ++m_canNotIndexClusters;
        return;
      }

      // tuple
      auto tuple = this->nTuple( "clusEff" );

      // initialise counters
      unsigned int nsignal          = 0;
      unsigned int nreconstructible = 0;
      unsigned int nreconstructed   = 0;
      bool         is_reconstructed;

      if ( m_relations.exist() ) { // added for backward compatibility
        auto const& tableDigits = m_relations.get();

        auto mc2digitsTable = MC2DigitTable( *tableDigits, MC2DigitTable::inverse_tag{} );
        // create MCParticle -> CaloCluster relations table from inverse one
        auto mc2clusterTable = MC2ClusterTable( table, MC2ClusterTable::inverse_tag{} );

        // loop over all mc particles
        for ( const auto& mcp : mcparts ) {
          is_reconstructed = false;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Next MCParticle" << endmsg;
          // select signal as defined by m_PDGID and m_PDGIDParent
          if ( !mcp ) continue;
          if ( mcp->particleID().pid() != m_PDGID ) continue;
          auto mcp_mother = mcp->mother();
          if ( m_PDGIDParent && ( !mcp_mother || abs( mcp_mother->particleID().pid() ) != m_PDGIDParent ) ) continue;
          if ( m_PDGIDGrandparent &&
               ( !mcp_mother->mother() || abs( mcp_mother->mother()->particleID().pid() ) != m_PDGIDGrandparent ) )
            continue;
          if ( msgLevel( MSG::DEBUG ) )
            debug() << "MC particle, parent and grandparent ID are: " << m_PDGID << ", " << m_PDGIDParent << " and "
                    << m_PDGIDGrandparent << endmsg;

          // apply min ET cut
          if ( mcp->pt() < m_minET ) continue;
          nsignal++;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "MC particle has PT larger than: " << m_minET << endmsg;

          // check endVertex, also fills info into ntuple
          if ( not check_endVtx( mcp, tuple ) ) continue;

          // find E deposited in ECAL from the mcp and find total E deposited in ECAL
          // for the digits concerning the mcp (both are independent from the reconstructed clusters).
          float e_deposited = 0.0; // energy from the mcp deposited in ecal

          // iterate through all the digits of the mcp
          for ( const auto& [_, cell_id, weight] : mc2digitsTable.relations( mcp ) ) {
            e_deposited += weight; // accumulate weighted energy from the mcp
          }

          if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle has desposited E in ECAL" << endmsg;

          // check that the mcp has deposited most of its energy in the calo
          float f_mcp = e_deposited / mcp->momentum().e();
          if ( f_mcp < m_minMCfraction ) continue;

          // if there is any isolation criteria:
          // check that the major contribution to the calo digits (cross around max) is from the same mcp.
          if ( m_minIsolation > 0 ) {
            // from the neighbour cells arround the max deposited by the mcp,
            // accumulate the energy deposited and the energy from the digits in the ecal.
            const auto& [e_deposited_nb, e_calo_nb_digits] =
                neighborsEnergyOld( mc2digitsTable.relations( mcp ), digits, calo );
            float f_e = e_deposited_nb / e_calo_nb_digits;
            if ( f_e < m_minIsolation ) continue;
          }

          // if the mcp has deposited >90% of its energy in the calo and
          // passes the isolation criteria (if m_minIsolation > 0)
          // then mcp is reconstructible.
          nreconstructible++;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle is reco'ble" << endmsg;

          // fill tuple
          auto sc = tuple->column( "max_fMCP", f_mcp );

          // sanity check to see if there is any cluster associated to the MCP
          if ( mc2clusterTable.relations( mcp ).empty() ) continue;

          // find cluster with largest contribution from MCParticle, identified by cell_id of the seed.
          // 'weight' stores the total energy contribution of mcp to the cluster with seed 'cell_id'.
          const auto& [cell_id, weight] = largestWeight( mc2clusterTable.relations( mcp ) );

          // sanity check for valid cell_id
          if ( !isValid( cell_id ) ) continue;

          // find cluster matching MCParticle from cell_id
          auto it = cluster_index.find( cell_id ); // returns Iterator (see CaloClusters_v2)
          if ( it == cluster_index.end() ) {
            ++m_clNotFound;
            m_eff += is_reconstructed;
            continue;
          }

          // get cluster info
          auto cl      = *it;
          auto cl_pos  = cl.position();
          auto cl_e    = cl.e();
          auto cl_area = cell_id.area();
          auto eT_     = CaloDataFunctor::EnergyTransverse{ &calo };
          auto cl_et   = eT_( &cl );

          // fraction of cluster energy from this MCParticle
          float f_cl = ( cl_e > 0. ) ? weight / cl_e : 0.;
          // fraction of the MCParticle deposit inside the cluster
          float f_in_cl = weight / e_deposited;

          // fill tuple
          sc &= tuple->column( "f_cl", f_cl );
          sc &= tuple->column( "cl_e", cl_e );
          sc &= tuple->column( "cl_et", cl_et );
          sc &= tuple->column( "cl_x", cl_pos.x() );
          sc &= tuple->column( "cl_y", cl_pos.y() );
          sc &= tuple->column( "cl_z", cl_pos.z() );
          sc &= tuple->column( "cl_area", cl_area );
          sc &= tuple->column( "fMCP_in_cl", f_in_cl );

          // write tuple
          sc.andThen( [&] { return tuple->write(); } ).ignore();

          // apply reco'ed threshold m_minMatchFraction
          if ( ( f_cl >= m_minMatchFraction ) && ( f_in_cl >= m_minMCInCluster ) ) {
            nreconstructed++;
            is_reconstructed = true;
            if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle is reco'ed" << endmsg;
          }
          m_eff += is_reconstructed;
        }
      }

      if ( m_links.exist() ) {
        auto const* linksDigits = m_links.get();

        // create MCParticle -> CaloDigit relations table from inverse one
        std::vector<std::map<unsigned int, float>> weightForParticle;
        linksDigits->applyToAllLinks( [&weightForParticle]( auto id, auto mcPartKey, auto weight ) {
          if ( weightForParticle.size() <= mcPartKey ) { weightForParticle.resize( mcPartKey + 1 ); }
          weightForParticle[mcPartKey].insert( std::pair<unsigned int, float>( id, weight ) );
        } );

        // create MCParticle -> CaloCluster relations table from inverse one
        auto mc2clusterTable = MC2ClusterTable( table, MC2ClusterTable::inverse_tag{} );

        // loop over all mc particles
        for ( const auto& mcp : mcparts ) {
          is_reconstructed = false;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Next MCParticle" << endmsg;
          // select signal as defined by m_PDGID and m_PDGIDParent
          if ( !mcp ) continue;
          if ( mcp->particleID().pid() != m_PDGID ) continue;
          auto mcp_mother = mcp->mother();
          if ( m_PDGIDParent && ( !mcp_mother || abs( mcp_mother->particleID().pid() ) != m_PDGIDParent ) ) continue;
          if ( m_PDGIDGrandparent &&
               ( !mcp_mother->mother() || abs( mcp_mother->mother()->particleID().pid() ) != m_PDGIDGrandparent ) )
            continue;
          if ( msgLevel( MSG::DEBUG ) )
            debug() << "MC particle, parent and grandparent ID are: " << m_PDGID << ", " << m_PDGIDParent << " and "
                    << m_PDGIDGrandparent << endmsg;

          // apply min ET cut
          if ( mcp->pt() < m_minET ) continue;
          nsignal++;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "MC particle has PT larger than: " << m_minET << endmsg;

          // check endVertex, also fills info into ntuple
          if ( not check_endVtx( mcp, tuple ) ) continue;

          // find E deposited in ECAL from the mcp and find total E deposited in ECAL
          // for the digits concerning the mcp (both are independent from the reconstructed clusters).
          float e_deposited = 0.0; // energy from the mcp deposited in ecal

          // iterate through all the digits of the mcp
          if ( weightForParticle.size() > (unsigned int)mcp->key() ) {
            for ( auto const& pair : weightForParticle.at( mcp->key() ) ) { e_deposited += pair.second; }
          } else
            continue;

          if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle has desposited E in ECAL" << endmsg;

          // check that the mcp has deposited most of its energy in the calo
          float f_mcp = e_deposited / mcp->momentum().e();
          if ( f_mcp < m_minMCfraction ) continue;

          // if there is any isolation criteria:
          // check that the major contribution to the calo digits (cross around max) is from the same mcp.
          if ( m_minIsolation > 0 ) {
            // from the neighbour cells arround the max deposited by the mcp,
            // accumulate the energy deposited and the energy from the digits in the ecal.
            const auto& [e_deposited_nb, e_calo_nb_digits] =
                neighborsEnergy( weightForParticle.at( mcp->key() ), digits, calo );
            float f_e = e_deposited_nb / e_calo_nb_digits;
            if ( f_e < m_minIsolation ) continue;
          }

          // if the mcp has deposited >90% of its energy in the calo and
          // passes the isolation criteria (if m_minIsolation > 0)
          // then mcp is reconstructible.
          nreconstructible++;
          if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle is reco'ble" << endmsg;

          // fill tuple
          auto sc = tuple->column( "max_fMCP", f_mcp );

          // sanity check to see if there is any cluster associated to the MCP
          if ( mc2clusterTable.relations( mcp ).empty() ) continue;

          // find cluster with largest contribution from MCParticle, identified by cell_id of the seed.
          // 'weight' stores the total energy contribution of mcp to the cluster with seed 'cell_id'.
          const auto& [cell_id, weight] = largestWeight( mc2clusterTable.relations( mcp ) );

          // sanity check for valid cell_id
          if ( !isValid( cell_id ) ) continue;

          // find cluster matching MCParticle from cell_id
          auto it = cluster_index.find( cell_id ); // returns Iterator (see CaloClusters_v2)
          if ( it == cluster_index.end() ) {
            ++m_clNotFound;
            m_eff += is_reconstructed;
            continue;
          }

          // get cluster info
          auto cl      = *it;
          auto cl_pos  = cl.position();
          auto cl_e    = cl.e();
          auto cl_area = cell_id.area();
          auto eT_     = CaloDataFunctor::EnergyTransverse{ &calo };
          auto cl_et   = eT_( &cl );

          // fraction of cluster energy from this MCParticle
          float f_cl = ( cl_e > 0. ) ? weight / cl_e : 0.;
          // fraction of the MCParticle deposit inside the cluster
          float f_in_cl = weight / e_deposited;

          // fill tuple
          sc &= tuple->column( "f_cl", f_cl );
          sc &= tuple->column( "cl_e", cl_e );
          sc &= tuple->column( "cl_et", cl_et );
          sc &= tuple->column( "cl_x", cl_pos.x() );
          sc &= tuple->column( "cl_y", cl_pos.y() );
          sc &= tuple->column( "cl_z", cl_pos.z() );
          sc &= tuple->column( "cl_area", cl_area );
          sc &= tuple->column( "fMCP_in_cl", f_in_cl );

          // write tuple
          sc.andThen( [&] { return tuple->write(); } ).ignore();

          // apply reco'ed threshold m_minMatchFraction
          if ( ( f_cl >= m_minMatchFraction ) && ( f_in_cl >= m_minMCInCluster ) ) {
            nreconstructed++;
            is_reconstructed = true;
            if ( msgLevel( MSG::DEBUG ) ) debug() << "MCParticle is reco'ed" << endmsg;
          }
          m_eff += is_reconstructed;
        }
      }

      // update counters
      m_signal += nsignal;
      m_reconstructible += nreconstructible;
      m_reconstructed += nreconstructed;
    }

  private:
    // properties
    Gaudi::Property<int>   m_PDGID{ this, "PDGID", 22 };
    Gaudi::Property<int>   m_PDGIDParent{ this, "PDGIDParent", 511 };
    Gaudi::Property<int>   m_PDGIDGrandparent{ this, "PDGIDGrandparent", 0 };
    Gaudi::Property<float> m_minMCfraction{ this, "minMCfraction", 0.9 };
    Gaudi::Property<float> m_minMatchFraction{ this, "minMatchFraction", 0.9 };
    Gaudi::Property<float> m_minET{ this, "minET", 50. };
    Gaudi::Property<float> m_minIsolation{ this, "minIsolation", 0. };
    Gaudi::Property<float> m_minMCInCluster{ this, "minMCInCluster", 0.9 };
    Gaudi::Property<float> m_minEndVtxZ{ this, "minEndVtxZ", 7000. };

    DataObjectReadHandle<LHCb::LinksByKey> m_links{
        this, "inputRelationsDigit", Links::location( LHCb::CaloDigitLocation::Default + "2MCParticles" ) };
    DataObjectReadHandle<LHCb::CaloFuture2MC::DigitTable> m_relations{
        this, "inputRelationsDigitOld", "Relations/" + LHCb::CaloDigitLocation::Default };

    // counters
    mutable Gaudi::Accumulators::StatCounter<unsigned int>     m_signal{ this, "# signal" };
    mutable Gaudi::Accumulators::StatCounter<unsigned int>     m_reconstructible{ this, "# reconstructible" };
    mutable Gaudi::Accumulators::StatCounter<unsigned int>     m_reconstructed{ this, "# reconstructed" };
    mutable Gaudi::Accumulators::BinomialCounter<unsigned int> m_eff{ this, "reco efficiency" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::INFO> m_multVtx{ this, "More than 1 endVertex found for signal", 0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_clNotFound{
        this, "CellID from relations table not found in clusters" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_canNotIndexClusters{ this,
                                                                               "Failed to index cluster container" };

    bool check_endVtx( LHCb::MCParticle* mcp, Tuple tuple ) const {
      // get endVertices
      auto endVertices = mcp->endVertices();
      if ( endVertices.empty() ) {
        if ( msgLevel( MSG::DEBUG ) ) { debug() << "MCParticle has empty endVertices" << endmsg; }
        // no endVertices means no interaction before 50m -> not reconstructible
        return false;
      } else {
        if ( msgLevel( MSG::DEBUG ) ) { debug() << "MCParticle has endVertices" << endmsg; }
        if ( endVertices.size() > 1 ) { ++m_multVtx; }

        // get last endVertex, as it's the decay vertex
        auto endVertex = endVertices.back();
        auto pos       = endVertex->position();

        // only MCParticles with conversion after m_minEndVtxZ are reconstructible
        if ( pos.z() < m_minEndVtxZ ) return false;

        // fill endVtx info
        auto sc = tuple->column( "endVtx_X", pos.x() );
        sc &= tuple->column( "endVtx_Y", pos.y() );
        sc &= tuple->column( "endVtx_Z", pos.z() );
        sc &= tuple->column( "endVtx_type", endVertex->type() );

        return true;
      }
    }

    using Result = std::pair<LHCb::Detector::Calo::CellID, float>;

    template <typename Range>
    Result largestWeight( const Range& range ) const {
      return std::accumulate( range.begin(), range.end(), Result{ {}, 0. },
                              [&]( Result current, const auto& relation ) {
                                const LHCb::Detector::Calo::CellID cellid = relation.to();
                                const auto weight = relation.weight(); // "weight" of the relation
                                if ( msgLevel( MSG::DEBUG ) ) {
                                  debug() << " -- Cell ID: " << cellid << endmsg;
                                  debug() << " -- weight: " << weight << endmsg;
                                }
                                return weight <= current.second ? current : Result{ cellid, weight };
                              } );
    }

    template <typename Range>
    std::pair<float, float> neighborsEnergyOld( const Range& range, const LHCb::Event::Calo::Digits& digits,
                                                const DeCalorimeter& calo ) const { // added for backward compatibility
      // select the most energetic cell of the mcp and find its neighbor cell_ids
      auto& [p_, cell_id_m, w_] = range[range.size() - 1];
      float e_deposited_nb      = 0;
      float e_calo_nb_digits    = 0;
      for ( auto cell : calo.neighborCells( cell_id_m ) ) {
        auto pos = find( range.begin(), range.end(), cell );
        if ( pos != range.end() ) {
          auto [_, cell_id_, e_] = *pos;
          e_deposited_nb += e_;             // take the weight position of the range that is equal to cell
          auto digit = digits.find( cell ); // find the same digit from the ecal
          if ( !digit ) continue;
          e_calo_nb_digits += digit->energy(); // accumulate energy from the calo digits
        }
      }
      return std::pair( e_deposited_nb, e_calo_nb_digits );
    }

    std::pair<float, float> neighborsEnergy( const std::map<unsigned int, float>& range,
                                             const LHCb::Event::Calo::Digits&     digits,
                                             const DeCalorimeter&                 calo ) const {
      // select the most energetic cell of the mcp and find its neighbor cell_ids
      auto cell_id = 0;
      auto cell_e  = 0;
      for ( auto const& pair : range ) {
        if ( pair.second > cell_e ) {
          cell_e  = pair.second;
          cell_id = pair.first;
        }
      }
      LHCb::Detector::Calo::CellID cell_id_m = LHCb::Detector::Calo::CellID( cell_id );

      float e_deposited_nb   = 0;
      float e_calo_nb_digits = 0;
      for ( auto cell : calo.neighborCells( cell_id_m ) ) {
        auto pos = range.find( cell.index() );
        if ( pos != range.end() ) {
          e_deposited_nb += pos->second;    // take the weight position of the range that is equal to cell
          auto digit = digits.find( cell ); // find the same digit from the ecal
          if ( !digit ) continue;
          e_calo_nb_digits += digit->energy(); // accumulate energy from the calo digits
        }
      }
      return std::pair( e_deposited_nb, e_calo_nb_digits );
    }
  };
  DECLARE_COMPONENT_WITH_ID( ClusterEfficiency, "CaloClusterEfficiency" )
} // namespace LHCb::Calo::Algorithms

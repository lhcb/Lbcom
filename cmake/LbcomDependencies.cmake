###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

if(NOT COMMAND lhcb_find_package)
    # Look for LHCb find_package wrapper
    find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
    if(LHCbFindPackage_FILE)
        include(${LHCbFindPackage_FILE})
    else()
        # if not found, use the standard find_package
        macro(lhcb_find_package)
            find_package(${ARGV})
        endmacro()
    endif()
endif()

# Lbcom does not have public dependencies, but LHCb is needed
# for the CMake functions used in LbcomConfig.cmake
lhcb_find_package(LHCb REQUIRED)

# -- Private dependencies
if(WITH_Lbcom_PRIVATE_DEPENDENCIES)
    find_package(AIDA REQUIRED)
    find_package(Boost CONFIG REQUIRED headers)
    find_package(GSL REQUIRED)
    find_package(ROOT REQUIRED GenVector Hist)
endif()

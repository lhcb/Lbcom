2017-09-04 LbcomSys v20r6p1
---
This version uses Gaudi v28r2 and LHCb v42r6p1 (and LCG_88 with ROOT 6.08.06)

This version is released on `2017-patches` branch.

Identical to v20r6 but with updated dependency to LHCb v42r6p1

2022-11-23 Lbcom v34r2
===

This version uses
LHCb [v54r2](../../../../LHCb/-/tags/v54r2),
Detector [v1r6](../../../../Detector/-/tags/v1r6),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9) and
LCG [101a_LHCB_7](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Built relative to Lbcom [v34r1](/../../tags/v34r1), with the following changes:

### Other

- ~Calo ~"MC checking" | CaloClusterEfficiency - Suppress event loop INFO message, !637 (@jonrob)
- ~Core | Dropped direct usage of Gaudi::Functional algorithms, !638 (@sponce)
- Prepare Lbcom v34r1p1, !641 (@rmatev)
- Add missing link, !640 (@clemenci)
- Add missing links to LHCbAlgsLib, !639 (@clemenci)
- Dropped unused class MuonPad2MCTool, !633 (@sponce)

2023-07-16 Lbcom v34r12
===

This version uses
LHCb [v54r12](../../../../LHCb/-/tags/v54r12),
Gaudi [v36r14](../../../../Gaudi/-/tags/v36r14),
Detector [v1r16](../../../../Detector/-/tags/v1r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Lbcom [v34r11](/../../tags/v34r11), with the following changes:

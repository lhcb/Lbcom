2024-11-01 Lbcom v35r16
===

This version uses
LHCb [v55r16](../../../../LHCb/-/tags/v55r16),
Gaudi [v38r1p1](../../../../Gaudi/-/tags/v38r1p1),
Detector [v1r36](../../../../Detector/-/tags/v1r36) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Lbcom [v35r15](/../../tags/v35r15), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- Prepare Lbcom v35r16, !764 (@msaur) !WARNING: re-released due to a wrong Gaudi tag

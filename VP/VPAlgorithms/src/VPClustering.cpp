/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/VPCluster.h"
#include "Event/VPDigit.h"
#include "GaudiKernel/Point3DTypes.h"
#include "Kernel/VPDataFunctor.h"
#include "LHCbAlgs/Transformer.h"
#include "VPDet/DeVP.h"
#include "VPDet/DeVPSensor.h"
#include <numeric>

namespace {
  //  Check if pixel is on the edge of a chip
  bool isEdge( const LHCb::VPDigit& digit ) {
    auto id = digit.channelID();
    return id.col() == LHCb::Detector::VPChannelID::ColumnID{ 255 }
               ? id.chip() < LHCb::Detector::VPChannelID::ChipID{ 2 }
           : id.col() == LHCb::Detector::VPChannelID::ColumnID{ 0 }
               ? id.chip() > LHCb::Detector::VPChannelID::ChipID{ 0 }
               : false;
  }
} // namespace

namespace LHCb {

  /**
   *  @author Daniel Hynds
   *  @date   2013-08-13
   */
  struct VPClustering
      : Algorithm::Transformer<VPClusters( const VPDigits&, const DeVP& ), Algorithm::Traits::usesConditions<DeVP>> {
    VPClustering( const std::string& name, ISvcLocator* pSvcLocator );
    VPClusters operator()( const VPDigits& digits, const DeVP& det ) const override;
  };

  DECLARE_COMPONENT_WITH_ID( VPClustering, "VPClustering" )

} // namespace LHCb

LHCb::VPClustering::VPClustering( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{ name,
                   pSvcLocator,
                   { KeyValue{ "DigitContainer", LHCb::VPDigitLocation::Default },
                     KeyValue{ "DeVPLocation", DeVPLocation::Default } },
                   { "VPClusterContainer", LHCb::VPClusterLocation::Default } } {}

namespace {
  bool pointToChannel( const DeVPSensor& sensor, const Gaudi::XYZPoint& localPoint,
                       LHCb::Detector::VPChannelID& channel, std::pair<double, double>& fraction ) {
    // Check if the point is in the active area of the sensor.
    if ( !sensor.isInActiveArea( localPoint ) ) return false;
    fraction.first  = 0.;
    fraction.second = 0.;
    // Set the sensor number.
    channel.setSensor( sensor.sensorNumber() );
    const double step = sensor.chipSize() + sensor.interChipDist();
    double       x0   = 0.;
    for ( unsigned int i = 0; i < sensor.numChips(); ++i ) {
      if ( localPoint.x() < x0 + step ) {
        // Set the chip number.
        channel.setChip( LHCb::Detector::VPChannelID::ChipID{ i } );
        // Calculate the column number.
        const double       x    = localPoint.x() - x0;
        const double       fcol = x / sensor.pixelSize() - 0.5;
        const unsigned int icol = fcol > 0. ? int( fcol ) : 0;
        // Set column and inter-pixel fraction.
        if ( icol <= 0 ) {
          channel.setCol( LHCb::Detector::VPChannelID::ColumnID{ 0 } );
          if ( 0 == i ) {
            if ( fcol > 0. ) fraction.first = fcol;
          } else {
            // First column has elongated pixels.
            const double pitch = 0.5 * ( sensor.pixelSize() + sensor.interChipPixelSize() );
            fraction.first     = x / pitch;
          }
        } else if ( icol >= sensor.numColumns() - 1 ) {
          channel.setCol( LHCb::Detector::VPChannelID::ColumnID{ sensor.numColumns() - 1 } );
          if ( i == sensor.numChips() - 1 ) {
            fraction.first = fcol - icol;
          } else {
            // Last column has elongated pixels.
            if ( x < sensor.chipSize() ) {
              // This point is assigned to the last but one pixel.
              channel.setCol( LHCb::Detector::VPChannelID::ColumnID{ sensor.numColumns() - 2 } );
              const double pitch = 0.5 * ( sensor.pixelSize() + sensor.interChipPixelSize() );
              fraction.first     = 1. - ( sensor.chipSize() - x ) / pitch;
            } else {
              // Point is in inter-chip region.
              fraction.first = ( x - sensor.chipSize() ) / sensor.interChipPixelSize();
            }
          }
        } else {
          channel.setCol( LHCb::Detector::VPChannelID::ColumnID{ icol } );
          fraction.first = fcol - icol;
          if ( icol == sensor.numColumns() - 2 && i < sensor.numChips() - 1 ) {
            fraction.first *= sensor.pixelSize() / sensor.interChipPixelSize();
          }
        }
        // Set the row and inter-pixel fraction.
        const double       frow = localPoint.y() / sensor.pixelSize() - 0.5;
        const unsigned int irow = frow > 0. ? int( frow ) : 0;
        if ( irow <= 0 ) {
          channel.setRow( LHCb::Detector::VPChannelID::RowID{ 0 } );
          if ( frow > 0. ) fraction.second = frow;
        } else {
          channel.setRow( LHCb::Detector::VPChannelID::RowID{ irow } );
          fraction.second = frow - irow;
        }
        break;
      }
      x0 += step;
    }
    return true;
  }

} // namespace

//=============================================================================
// Main execution
//=============================================================================
LHCb::VPClusters LHCb::VPClustering::operator()( const LHCb::VPDigits& digits, const DeVP& det ) const {

  // check that the digits are sorted by channel ID.
  if ( !std::is_sorted( digits.begin(), digits.end(), VPDataFunctor::Less_by_Channel<const LHCb::VPDigit*>() ) ) {
    throw GaudiException{ "got unsorted digits???", __PRETTY_FUNCTION__, StatusCode::FAILURE };
  }

  // Create container
  VPClusters clusters;
  auto       itBegin = digits.begin();
  auto       itLast  = std::prev( digits.end() );

  std::vector<const LHCb::VPDigit*> cluster;
  cluster.reserve( 100 );
  std::vector<LHCb::Detector::VPChannelID> pixels;
  pixels.reserve( 100 );
  // Keep track of used digits.
  std::vector<bool> isUsed( digits.size(), false );
  // Loop over digits.
  for ( auto itSeed = digits.begin(); itSeed != digits.end(); ++itSeed ) {
    if ( isUsed[itSeed - itBegin] ) continue;
    // Create a new cluster with this seed.
    cluster.clear();
    cluster.push_back( *itSeed );
    // Store the sensor of this cluster.
    const auto sensorNumber = ( *itSeed )->channelID().sensor();
    // Tag the digit as used.
    isUsed[itSeed - itBegin] = true;
    // Look for neighbouring hits until the cluster size stops changing.
    bool keepSearching = ( itSeed != itLast );
    // Start the search on the next unused pixel.
    auto itCandBegin = itSeed;
    while ( isUsed[itCandBegin - itBegin] ) {
      if ( itCandBegin == itLast ) break;
      ++itCandBegin;
    }
    while ( keepSearching ) {
      keepSearching = false;
      // Loop over the digits which are already pare of the cluster.
      for ( unsigned int i = 0; i < cluster.size(); ++i ) {
        // Store some properties of this digit.
        const bool edge = isEdge( *cluster[i] );
        const auto chip = cluster[i]->channelID().chip();
        const auto row  = cluster[i]->channelID().row();
        const auto col  = cluster[i]->channelID().col();
        // Loop down the stored digits until new pixels cannot be part of
        // cluster.
        for ( auto iCand = itCandBegin; iCand != digits.end(); ++iCand ) {
          if ( isUsed[iCand - itBegin] ) continue;
          // Check if the candidate is on the same sensor.
          if ( sensorNumber != ( *iCand )->channelID().sensor() ) break;
          if ( !edge ) {
            if ( chip != ( *iCand )->channelID().chip() ) break; // Next hit not on same chip
            // Skip pixels not neighbouring in y.
            if ( abs( int( row ) - int( ( *iCand )->channelID().row() ) ) > 1 ) continue;
            if ( abs( int( col ) - int( ( *iCand )->channelID().col() ) ) > 1 ) continue; // Too far away to be added
            // Add pixel and tag it as used.
            cluster.push_back( *iCand );
            isUsed[iCand - itBegin] = true;
            keepSearching           = true;
            break;
          } else {
            // Deal with edge pixels
            if ( chip == ( *iCand )->channelID().chip() ) {
              // On the same chip
              // Skip pixels not neighbouring in y.
              if ( abs( int( row ) - int( ( *iCand )->channelID().row() ) ) > 1 ) continue;
              if ( abs( int( col ) - int( ( *iCand )->channelID().col() ) ) > 1 ) continue; // Too far away to be added
              // Add pixel and tag it as used.
              cluster.push_back( *iCand );
              isUsed[iCand - itBegin] = true;
              keepSearching           = true;
              break;
            } else {
              // Not on the same chip
              if ( !isEdge( **iCand ) ) break; // No hits on neighbouring edge
              // Skip pixels not neighbouring in y.
              if ( abs( int( row ) - int( ( *iCand )->channelID().row() ) ) > 1 ) continue;
              // Add pixel and tag it as used.
              cluster.push_back( *iCand );
              isUsed[iCand - itBegin] = true;
              keepSearching           = true;
              break;
            }
          } // End of edge pixel check
        }
      }
    }
    // Calculate the barycentre of the cluster.
    const DeVPSensor& vp_sensor = det.sensor( cluster[0]->channelID().sensor() );
    pixels.clear();
    std::transform( clusters.begin(), clusters.end(), std::back_inserter( pixels ),
                    []( const LHCb::VPCluster* cluster ) { return cluster->channelID(); } );
    auto               xy      = std::accumulate( pixels.begin(), pixels.end(), std::pair( 0., 0. ),
                                                  [&]( std::pair<double, double> xy, const LHCb::Detector::VPChannelID& channel ) {
                                 Gaudi::XYZPoint pixel = vp_sensor.channelToLocalPoint( channel );
                                 xy.first += pixel.x();
                                 xy.second += pixel.y();
                                 return xy;
                               } );
    const unsigned int nPixels = pixels.size();
    Gaudi::XYZPoint    point( xy.first / nPixels, xy.second / nPixels, 0. );
    // Get the channel ID and inter-pixel fractions of the barycentre.
    LHCb::Detector::VPChannelID id;
    std::pair<double, double>   frac;
    if ( !pointToChannel( vp_sensor, point, id, frac ) ) continue;
    // Make sure there isn't already a cluster with the same channel ID.
    if ( clusters.object( id ) ) {
      warning() << "Duplicated channel ID " << id << endmsg;
      continue;
    }
    // Calculate the position in global coordinates.
    point = vp_sensor.localToGlobal( point );
    // Add the cluster to the list.
    LHCb::VPCluster* newCluster = new LHCb::VPCluster( frac, point.x(), point.y(), point.z(), pixels );
    clusters.insert( newCluster, id );
  }
  return clusters;
}

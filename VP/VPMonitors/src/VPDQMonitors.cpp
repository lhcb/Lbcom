/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/VPFullCluster.h"
#include "Kernel/VPConstants.h"

#include "DetDesc/GenericConditionAccessorHolder.h"

#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators/Histogram.h"
#include "Gaudi/Accumulators/HistogramArray.h"

#include <algorithm>
#include <cmath>
#include <string>
#include <utility>
#include <vector>

// May need this
#include "Event/RawBank.h"
#include "Event/RawEvent.h"

#include <iostream>
#include <sstream>

/* @class VPDQMonitors VPDQMonitors.cpp
 *
 * * Class for plotting
 *   - Number of clusters per event using a mutable work around
 *
 * @author D.A.Friday
 * @date   2023-05-11
 */

namespace {

  constexpr uint get_sensorNr( uint sensor ) { return sensor % VP::NSensorsPerModule; }
  constexpr uint get_asicNr( uint sensorNr, uint chipNr ) { return 3 * sensorNr + chipNr; }

} // namespace

namespace LHCb {
  class VPDQMonitors : public Algorithm::Consumer<void( const std::vector<VPFullCluster>&, const RawEvent& )> {

  public:
    VPDQMonitors( const std::string& name, ISvcLocator* pSvcLocator );

    void operator()( const std::vector<VPFullCluster>&, const RawEvent& ) const override;

  private:
    // Builder for all histograms
    mutable Gaudi::Accumulators::Histogram<1> m_clusters_per_event{
        this, "VPClusPerEvent", "VP Clusters per Event; Clusters/event;", { 1001, -5, 10005, "X" } };

    mutable Gaudi::Accumulators::Histogram<2> m_asic_map{ this,
                                                          "VPAsicmap",
                                                          "VP Asic Map; Clusters/event;",
                                                          { VP::NModules, -0.5, VP::NModules - 0.5, "X" },
                                                          { 12, -0.5, 11.5, "Y" } };

    mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<1>, 12> m_asics{
        this,
        []( int n ) { return fmt::format( "VPAsic{}", n ); },
        []( int n ) { return fmt::format( "Asic {}; Clusters/event; Module", n ); },
        { VP::NModules, -0.5, VP::NModules - 0.5, "X" } };

    mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<1>, 4> m_sensors{
        this,
        []( int n ) { return fmt::format( "VPSensor{}", n ); },
        []( int n ) { return fmt::format( "Sensor {}; Clusters/event; Module", n ); },
        { VP::NModules, -0.5, VP::NModules - 0.5, "X" } };

    mutable Gaudi::Accumulators::Histogram<1> m_sensor_in{ this,
                                                           "VPSensorIn",
                                                           "Sensors Inner (Clusters/event); Module;",
                                                           { VP::NModules, -0.5, VP::NModules - 0.5, "X" } };

    mutable Gaudi::Accumulators::Histogram<1> m_sensor_out{ this,
                                                            "VPSensorOut",
                                                            "Sensors Outer (Clusters/event); Module;",
                                                            { VP::NModules, -0.5, VP::NModules - 0.5, "X" } };

    mutable Gaudi::Accumulators::Histogram<1> m_sensor_all{
        this, "VPSensors", "Sensors (Clusters/event); Module;", { VP::NModules, -0.5, VP::NModules - 0.5, "X" } };

    // Error bank plots

    mutable Gaudi::Accumulators::Histogram<1> m_error_banks_daq{
        this,
        "DAQErrorBanks",
        "DAQ Error Banks",
        { 10,
          -0.5,
          9.5,
          "Error Type",
          { "FragmentThrottled", "BXIDCorrupted", "SyncBXIDCorrupted", "FragmentMissing", "FragmentTruncated",
            "IdleBXIDCorrupted", "FragmentMalformed", "EVIDJumped", "AlignFifoFull", "FEfragSizeWrong" } } };
    mutable Gaudi::Accumulators::Histogram<1> m_error_banks_velo{
        this,
        "VPErrorBanks",
        "VELO Error Banks",
        { 6,
          -0.5,
          5.5,
          "Error Type",
          { "Many SPPS", "Out Format", "Out Reclock", "Cluster", "Cluster Ready", "Undefined" } } };

    mutable Gaudi::Accumulators::Histogram<2> m_error_banks_velo_2d{
        this,
        "VPErrorBanks2D",
        "VELO Error Banks by Module",
        { 6,
          -0.5,
          5.5,
          "Error Type",
          { "Many SPPS", "Out Format", "Out Reclock", "Cluster", "Cluster Ready", "Undefined" } },
        { 52, -0.5, 51.5, "Module" } };
  }; // class VPDQMonitors
} // namespace LHCb
// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( LHCb::VPDQMonitors, "VPDQMonitors" )

// ===============================================================
// Constructor
// ===============================================================
LHCb::VPDQMonitors::VPDQMonitors( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {
                    KeyValue{ "ClusterLocation", LHCb::VPFullClusterLocation::Default },
                    KeyValue{ "RawEventLocation", LHCb::RawEventLocation::VeloCluster },
                } ) {}

// ===============================================================
// Main
// ===============================================================

void LHCb::VPDQMonitors::operator()( const std::vector<LHCb::VPFullCluster>& vpClusters,
                                     const LHCb::RawEvent&                   rawEvent ) const {

  /////////////////////////////////
  /// ERRORS

  // Get VELO Error Banks, DAQ Error Banks
  const auto& error_banks               = rawEvent.banks( LHCb::RawBank::VeloError );
  const auto& DaqErrorFragmentThrottled = rawEvent.banks( LHCb::RawBank::DaqErrorFragmentThrottled ); // 89
  const auto& DaqErrorBXIDCorrupted     = rawEvent.banks( LHCb::RawBank::DaqErrorBXIDCorrupted );     // 90
  const auto& DaqErrorSyncBXIDCorrupted = rawEvent.banks( LHCb::RawBank::DaqErrorSyncBXIDCorrupted ); // 91
  const auto& DaqErrorFragmentMissing   = rawEvent.banks( LHCb::RawBank::DaqErrorFragmentMissing );   // 92
  const auto& DaqErrorFragmentTruncated = rawEvent.banks( LHCb::RawBank::DaqErrorFragmentTruncated ); // 93
  const auto& DaqErrorIdleBXIDCorrupted = rawEvent.banks( LHCb::RawBank::DaqErrorIdleBXIDCorrupted ); // 94
  const auto& DaqErrorFragmentMalformed = rawEvent.banks( LHCb::RawBank::DaqErrorFragmentMalformed ); // 95
  const auto& DaqErrorEVIDJumped        = rawEvent.banks( LHCb::RawBank::DaqErrorEVIDJumped );        // 96
  const auto& DaqErrorAlignFifoFull     = rawEvent.banks( LHCb::RawBank::DaqErrorAlignFifoFull );     // 100
  const auto& DaqErrorFEfragSizeWrong   = rawEvent.banks( LHCb::RawBank::DaqErrorFEfragSizeWrong );   // 101

  // Error bank reference hex values
  const uint8_t VELO_ERR_RAM_MANY_SPPS     = 0x30;
  const uint8_t VELO_ERR_OUTPUT_FORMATTER  = 0x41;
  const uint8_t VELO_ERR_OUTPUT_RECLOCK    = 0x10;
  const uint8_t VELO_ERR_CLUSTER           = 0x50;
  const uint8_t VELO_ERR_CLUSTER_NOT_READY = 0x51;

  // Get sizes of the Daq banks
  std::vector<std::size_t> DaqSizeVector = { DaqErrorFragmentThrottled.size(), DaqErrorBXIDCorrupted.size(),
                                             DaqErrorSyncBXIDCorrupted.size(), DaqErrorFragmentMissing.size(),
                                             DaqErrorFragmentTruncated.size(), DaqErrorIdleBXIDCorrupted.size(),
                                             DaqErrorFragmentMalformed.size(), DaqErrorEVIDJumped.size(),
                                             DaqErrorAlignFifoFull.size(),     DaqErrorFEfragSizeWrong.size() };

  // Fill the DAQ Error histogram based on sizes
  for ( long unsigned int i = 0; i < DaqSizeVector.size(); i++ ) {
    if ( DaqSizeVector[i] > 0 ) {
      warning() << "WARNING!    Found DAQ Error!" << endmsg;
      ++m_error_banks_daq[i];
    }
  }

  // Loop over VELO Error RawBanks
  for ( const auto* error_bank : error_banks ) {

    auto         errors   = error_bank->range<uint32_t>(); // Breakdown banks
    auto         sourceID = error_bank->sourceID();        // Get bank source ID
    unsigned int module   = -1;                            // Initialise module variable

    // Figure out which module the sourceID corresponds to and set module accordingly
    for ( int mod = 0; mod < 52; ++mod ) {
      if ( mod % 2 == 0 ) {
        if ( sourceID == 6144 + ( mod * 2 ) || sourceID == 6145 + ( mod * 2 ) ) {
          module = mod;
          break;
        }
      } else {
        if ( sourceID == 4098 - 2 + ( mod * 2 ) || sourceID == 4099 - 2 + ( mod * 2 ) ) {
          module = mod;
          break;
        }
      }
    }

    // Bad module number warning
    if ( module == (unsigned int)-1 ) { warning() << "WARNING!    Module not found from source ID !" << endmsg; }

    // Loop through the bank in 32 bit chunks but break immediately after the first loop,
    // all VELO RawBank Error info needed is in the first chunk (for now)
    for ( uint32_t err : errors ) {

      // Get the relevant hex digits
      uint8_t errorHexDigits = ( err >> 16 ) & 0xFF;

      // Switch for error classification
      switch ( errorHexDigits ) {
      case VELO_ERR_RAM_MANY_SPPS:
        ++m_error_banks_velo[0];
        ++m_error_banks_velo_2d[{ 0, module }];
        break;
      case VELO_ERR_OUTPUT_FORMATTER:
        ++m_error_banks_velo[1];
        ++m_error_banks_velo_2d[{ 1, module }];
        break;
      case VELO_ERR_OUTPUT_RECLOCK:
        ++m_error_banks_velo[2];
        ++m_error_banks_velo_2d[{ 2, module }];
        break;
      case VELO_ERR_CLUSTER:
        ++m_error_banks_velo[3];
        ++m_error_banks_velo_2d[{ 3, module }];
        break;
      case VELO_ERR_CLUSTER_NOT_READY:
        ++m_error_banks_velo[4];
        ++m_error_banks_velo_2d[{ 4, module }];
        break;
      default:
        warning() << "WARNING!    Undefined VELO Error found!" << endmsg;
        ++m_error_banks_velo[5];
        ++m_error_banks_velo_2d[{ 5, module }];
        break;
      }
      break;
    }
  }

  /////////////////////////////////
  /// DQ

  // generate DQ plot
  ++m_clusters_per_event[vpClusters.size()];
  for ( const auto& vpCluster : vpClusters ) {
    // Get VP Channel IDs of pixels in cluster
    const LHCb::Detector::VPChannelID vpID = vpCluster.channelID();
    // Get cluster locations
    const auto mod    = vpID.module();
    const auto chipNr = to_unsigned( vpID.chip() );
    const auto sens   = to_unsigned( vpID.sensor() );
    const auto senNr  = get_sensorNr( sens );
    const auto asicNr = get_asicNr( senNr, chipNr );
    // Fill Asic/Sensor arrays
    ++m_asic_map[{ mod, asicNr }];
    ++m_asics[asicNr][mod];
    ++m_sensors[senNr][mod];
    ++m_sensor_all[mod];
    if ( senNr % 2 == 0 ) {
      ++m_sensor_in[mod];
    } else {
      ++m_sensor_out[mod];
    }
  }
};

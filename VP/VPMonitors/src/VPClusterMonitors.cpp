/*****************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#include "Event/RecVertex.h"

#include "Event/ODIN.h"
#include "Event/VPFullCluster.h"
#include "Kernel/VPConstants.h"

#include "VPDet/DeVP.h"

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "VPDet/DeVPSensor.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"

#include <unistd.h>

#include "Gaudi/Accumulators/Histogram.h"
#include "Gaudi/Accumulators/HistogramArray.h"

#include <string>
#include <vector>

/* @class  VPClusterMonitors VPClusterMonitors.h
 *
 * * Class for plotting:
 *   - Number of VP clusters
 *   - VP cluster size
 *
 * * How to use:
 *   from Configurables import VPClusFull, VPClusterMonitors
 *   gaudi_seqeuence = GaudiSequencer("VPClusterMonitorsSequencer")
 *   gaudi_seqeuence.Members += [VPClusFull(), VPClusterMonitors()]
 *
 * * Notes:
 *   - No. of clusters per superpixel per ASIC currently plotted stacking all modules rather than
 *     individually for all 624 ASICs
 *
 * @author T.H.McGrath
 * @date   2020-01-30
 *  * Update: Remove the TAE plots from clusters monitoring, currently for clusters monitoring still use DIM sources
 * @by Lanxing Li
 */

namespace {

  constexpr auto NAsics = VP::NSensorsPerModule * VP::NChipsPerSensor;

  // Transform sensor number to range [0,3] for plotting purposes
  constexpr uint get_sensorNr( uint sensor ) { return sensor % VP::NSensorsPerModule; }
  // Get asic in range [0, 623]
  constexpr uint get_asic( uint sensor, uint chipNr ) { return 3 * sensor + chipNr; }
  // Transform ASIC number to range [0,11] for plotting purposes
  constexpr uint get_asicNr( uint sensorNr, uint chipNr ) { return 3 * sensorNr + chipNr; }

} // anonymous namespace

// Sensors from DeVP
class zSensors {

public:
  // Constructor
  zSensors( const DeVP& det ) {
    auto totSensors = det.numberSensors();
    m_z.reserve( totSensors );
    for ( uint sensorNum = 0; sensorNum != totSensors; ++sensorNum ) {
      auto& DetSensor = det.sensor( LHCb::Detector::VPChannelID::SensorID( sensorNum ) );
      m_z.push_back( DetSensor.z() );
    } // will have to sort vector if sensor numbers not in ascending
  }
  zSensors() = default;

  // Get z-coordinate of sensor
  double z( uint nr ) const { return m_z.at( nr ); }

private:
  std::vector<double> m_z{};

}; // class zSensors

// Enumerators
enum struct OutputMode { Histograms, Tuples };
enum FillScheme {
  None = 0,
  BB,
  IsolatedBB,
  EE,
  EESpillover0,
  EESpillover1,
  EESpillover2,
  EEPrespill0,
  EEPrespill1,
  EEPrespill2,
  EB,
  BE,
  MaxFS
};

using BunchIDList = std::vector<unsigned>;

namespace LHCb {

  template <OutputMode outmode>
  class VPClusterMonitors
      : public Algorithm::Consumer<void( const ODIN&, const std::vector<VPFullCluster>&, const zSensors& ),
                                   Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, zSensors>> {

  public:
    VPClusterMonitors( const std::string& name, ISvcLocator* pSvcLocator );
    // Initialize
    StatusCode initialize() override;
    void       operator()( const ODIN&, const std::vector<VPFullCluster>&, const zSensors& ) const override;

  private:
    mutable int n_Vertices_per_event = 0;
    // Input variables
    // Gaudi::Property<bool> tuple {this, "Tuple", false, "Whether to produce ntuples instead of histograms"};
    Gaudi::Property<bool> m_allASICs{
        this, "AllASICs", false,
        "Whether to produce cluster maps for all ASICs separately (default: ASICs superposed for all modules)" };
    Gaudi::Property<bool>        debug{ this, "DebugMode", true, "Whether to produce extra plots for debugging" };
    Gaudi::Property<bool>        flag_EE{ this, "LocalFlag", true, "Whether to produce plots with only EE" };
    Gaudi::Property<std::string> m_dns{ this, "DNS", "mon01", "DIM node to subscribe to" };
    Gaudi::Property<int>         m_rebinValue{
        this, "RebinValue", 0, "Number of times the pixels will have a NxN rebin (should be powers of 2, max 2^8)" };
    Gaudi::Property<std::string> m_fsfile{
        this, "FSFile",
        "/group/online/tfc/LPCfiles/2022/8136/8136_fillingscheme.txt" /* defaulting to this just so code doesn't break
                                                                       */
        ,
        "Filling scheme text file for offline monitoring" };

    // Histogram objects

    using Histogram_1D      = Gaudi::Accumulators::Histogram<1>;
    using Histogram_2D      = Gaudi::Accumulators::Histogram<2>;
    using HistogramArray_1D = std::map<int, std::optional<Histogram_1D>>;
    using HistogramArray_2D = std::map<int, std::optional<Histogram_2D>>;

    // Book all plots

    // The only plot we always want to keep

    mutable HistogramArray_2D m_sp{};
    // Number of clusters per superpixel on ASIC

    // For the following plots, they are not necesasry (debug mode)

    mutable std::optional<Histogram_2D> m_fs;
    // Filling scheme for each event

    mutable HistogramArray_1D m_fsncl{};

    // Number of clusters per event for each fill category

    mutable HistogramArray_1D m_mnr_fs{};

    // Number of clusters per module for each fill category

    mutable std::optional<Histogram_1D> h1_v_per_event;
    // Number of P.V per event

    mutable std::optional<Histogram_1D> m_ncl;
    // Number of clusters per event i.e. size of the vpClusters vector per event

    mutable std::optional<Histogram_1D> m_ncld;
    // Number of clusters per event in range 0-50 with 51 bins

    mutable std::optional<Histogram_1D> m_mnr;

    // Number of clusters per module

    mutable std::optional<Histogram_1D> m_mz;
    // Number of clusters per z-location of module

    mutable std::optional<Histogram_1D> m_ch;
    // Number of clusters per chip

    mutable std::optional<Histogram_1D> m_se;
    // Number of clusters per sensor

    mutable std::optional<Histogram_1D> m_senr;
    // Number of clusters per sensor number

    mutable std::optional<Histogram_1D> m_as;
    // Number of clusters per ASIC

    mutable std::optional<Histogram_1D> m_asnr;
    // Number of clusters per ASIC number

    mutable std::optional<Histogram_1D> m_rad;
    // Number of clusters radius

    mutable std::optional<Histogram_2D> m_radz;
    // Number of clusters per radius per z-location of sensor

    mutable std::optional<Histogram_1D> m_pcl;
    // Number of pixels per cluster (cluster size)

    mutable std::optional<Histogram_2D> m_pmnr;
    // Number of cluster sizes per module

    mutable std::optional<Histogram_2D> m_pmz;
    // Number of cluster sizes per z-location of module

    mutable std::optional<Histogram_2D> m_pse;
    // Number of cluster sizes per sensor

    mutable std::optional<Histogram_2D> m_psenr;
    // Number of cluster sizes per sensor number

    mutable std::optional<Histogram_2D> m_pas;
    // Number of cluster sizes per ASIC

    mutable std::optional<Histogram_2D> m_pasnr;
    // Number of cluster sizes per ASIC number

    // Book all global xy cluster maps
    mutable std::optional<Histogram_2D> m_gxy_allC;
    // Number of clusters in global x,y for all modules on C side superposed

    mutable std::optional<Histogram_2D> m_gxy_allA;
    // Number of clusters in global x,y for all modules on A side superposed

    mutable HistogramArray_2D m_gxy_C{};
    // Number of clusters in global x,y for C side

    mutable HistogramArray_2D m_gxy_A{};
    // Number of clusters in global x,y for A side

    // Book Noise plot

    mutable HistogramArray_1D m_ncla{};
    // Number of clusters per event per asic
  };
} // namespace LHCb
//
// Declaration of the Algorithm Factory
using VPClusterMonitorsHistograms = LHCb::VPClusterMonitors<OutputMode::Histograms>;
using VPClusterMonitorsTuples     = LHCb::VPClusterMonitors<OutputMode::Tuples>;

DECLARE_COMPONENT_WITH_ID( VPClusterMonitorsHistograms, "VPClusterMonitors" )
DECLARE_COMPONENT_WITH_ID( VPClusterMonitorsTuples, "VPClusterMonitorsNT" )

// ===============================================================
// Constructor
// ===============================================================
template <OutputMode outmode>
LHCb::VPClusterMonitors<outmode>::VPClusterMonitors( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "ODINLocation", LHCb::ODINLocation::Default },
                  KeyValue{ "ClusterLocation", LHCb::VPFullClusterLocation::Default },
                  KeyValue{ "zSensorLocation",
#ifdef USE_DD4HEP
                            "/world:VPClusterMonitor-" + name + "-Sensors"
#else
                            "VPClusterMonitor-" + name + "-Sensors"
#endif
                  } } ) {
}

// ===============================================================
// Initialize
// ===============================================================
template <OutputMode outmode>
StatusCode LHCb::VPClusterMonitors<outmode>::initialize() {

  return Consumer::initialize().andThen( [&]() {
    // Register derived condition for DeVP
    addConditionDerivation<zSensors( DeVP const& )>( { DeVPLocation::Default }, inputLocation<zSensors>() );
    using Axis1D = Gaudi::Accumulators::Axis<double>; // used for booking map{;,Histogram}
    // Book histograms

    // Get the number of bins from property
    const int rebinValue =
        ( m_rebinValue.value() < 1 || m_rebinValue.value() > 8 ) ? 1 : pow( 2, m_rebinValue.value() ); // Used for rebin

    // Book the Histogram map

    for ( unsigned int isens = 0; isens < VP::NSensors; isens++ ) {
      // The Super-Pixel map which is always needed either for BB/EE.
      const std::string clusterName =
          "VPClusterMapOnMod" + std::to_string( int( isens / 4 ) ) + "Sens" + std::to_string( isens % 4 );
      m_sp.emplace( isens, std::nullopt );
      m_sp[isens].emplace( this, clusterName, clusterName, Axis1D{ VP::NColumns * 3 / rebinValue, 0, VP::NColumns * 3 },
                           Axis1D{ VP::NColumns / rebinValue, 0, VP::NColumns } );
    }

    if ( flag_EE ) {
      m_fs.emplace( this, "FillingScheme", "Filling Scheme; BxID; Filling Scheme", Axis1D{ 3564, 0.5, 3564.5 },
                    Axis1D{ 8, -0.5, 7.5 } );

      h1_v_per_event.emplace( this, "nVertex_event", "No. of vertices per event", Axis1D{ 21, -0.5, 20.5 } );

      m_ncl.emplace( this, "VPClustersPerEvent",
                     "Frequency of # of VP clusters per event (Frequency vs Clusters); Clusters/event;",
                     Axis1D{ 1001, -5, 10005 } );

      m_ncld.emplace( this, "VPClustersPerEventDetailed",
                      "Frequency of # of VP clusters per event (Frequency vs Clusters); Clusters/event;",
                      Axis1D{ 51, -0.5, 50.5 } );

      m_mnr.emplace( this, "VPClustersPerModule", "VP clusters per module (Clusters vs Module); Module; Clusters",
                     Axis1D{ VP::NModules, -0.5, VP::NModules - 0.5 } );

      m_mz.emplace( this, "VPClustersPerZ",
                    "VP clusters per z-coordinate of sensor (Clusters vs z (mm)); z (mm); Clusters",
                    Axis1D{ 105, -294, 756 } );

      m_ch.emplace( this, "VPClustersPerChip", "VP clusters per chip # (Clusters vs Chip #); Chip Number; Clusters",
                    Axis1D{ VP::NChipsPerSensor + 2, -1.5, VP::NChipsPerSensor + 0.5 } );

      m_se.emplace( this, "VPClustersPerSensor", "VP clusters per sensor (Clusters vs Sensor); Sensor; Clusters",
                    Axis1D{ VP::NSensors, -0.5, VP::NSensors - 0.5 } );

      m_senr.emplace( this, "VPClustersPerSensorNr",
                      "VP clusters per sensor # (Clusters vs Sensor #); Sensor Number; Clusters",
                      Axis1D{ VP::NSensorsPerModule + 2, -1.5, VP::NSensorsPerModule + 0.5 } );

      m_as.emplace( this, "VPClustersPerASIC", "VP clusters per ASIC (Clusters vs ASIC); ASIC; Clusters",
                    Axis1D{ NAsics * VP::NModules, 0, NAsics * VP::NModules } );

      m_asnr.emplace( this, "VPClustersPerASICNr", "VP clusters per ASIC # (Clusters vs ASIC #); ASIC Number; Clusters",
                      Axis1D{ NAsics, 0, NAsics } );

      m_rad.emplace( this, "VPClustersGlobalRadius", "VP clusters radius (Clusters vs Radius); Radius (mm); Clusters",
                     Axis1D{ 1000, 0, 150 } );

      m_radz.emplace( this, "VPClustersGlobalRadiusPerZ",
                      "VP clusters radius per z-coordinate of sensor (Radius vs z (mm)); z (mm); Radius (mm)",
                      Axis1D{ 105, -294, 756 }, Axis1D{ 1000, 0, 150 } );

      m_pcl.emplace( this, "VPClusterSize",
                     "Number of pixels per VP cluster (Frequency vs Cluster Size); Cluster Size;",
                     Axis1D{ 50, 0, 50 } );

      m_pmnr.emplace( this, "VPClusterSizeVSModule",
                      "Number of pixels per VP cluster per module (Cluster Size vs Module); Module; Cluster Size",
                      Axis1D{ VP::NModules, -0.5, VP::NModules - 0.5 }, Axis1D{ 10, 0.5, 10.5 } );

      m_pmz.emplace(
          this, "VPClusterSizeVSZ",
          "Number of pixels per VP cluster per z-coordinate of sensor (Cluster Size vs z (mm)); z (mm); Cluster "
          "Size",
          Axis1D{ 105, -294, 756 }, Axis1D{ 10, 0.5, 10.5 } );

      m_pse.emplace( this, "VPClusterSizeVSSensor",
                     "Number of pixels per VP cluster per sensor (Cluster Size vs Sensor); Sensor; Cluster Size",
                     Axis1D{ VP::NSensors, -0.5, VP::NSensors - 0.5 }, Axis1D{ 10, 0.5, 10.5 } );

      m_psenr.emplace(
          this, "VPClusterSizeVSSensorNr",
          "Number of pixels per VP cluster per sensor # (Cluster Size vs Sensor #); Sensor Number; Cluster "
          "Size",
          Axis1D{ VP::NSensorsPerModule + 2, -1.5, VP::NSensorsPerModule + 0.5 }, Axis1D{ 10, 0.5, 10.5 } );

      m_pas.emplace( this, "VPClusterSizeVSASIC",
                     "Number of pixels per VP cluster per ASIC (Cluster Size vs ASIC); ASIC; Cluster Size",
                     Axis1D{ NAsics * VP::NModules, 0, NAsics * VP::NModules }, Axis1D{ 10, 0.5, 10.5 } );

      m_pasnr.emplace( this, "VPClusterSizeVSASICNr",
                       "Number of pixels per VP cluster per ASIC # (Cluster Size vs ASIC #); ASIC Number; Cluster Size",
                       Axis1D{ NAsics, 0, NAsics }, Axis1D{ 10, 0.5, 10.5 } );

      m_gxy_allC.emplace( this, "VPClusterMapGlobalXYAllModulesCSide",
                          "VP cluster map for all C side Modules (y vs x); x (mm); y (mm)", Axis1D{ 800, -80, 80 },
                          Axis1D{ 600, -60, 60 } );

      m_gxy_allA.emplace( this, "VPClusterMapGlobalXYAllModulesASide",
                          "VP cluster map for all A side Modules (y vs x); x (mm); y (mm)", Axis1D{ 800, -80, 80 },
                          Axis1D{ 600, -60, 60 } );

      for ( int i = 0; i < 11; i++ ) { // Book for clusters per filling type
        auto title_fs = { "BB", "IsoBB", "EE", "SO0EE", "SO1EE", "SO2EE", "PS0EE", "PS1EE", "PS2EE", "EB", "BE" };
        m_fsncl.emplace( i, std::nullopt );
        m_fsncl[i].emplace( this, fmt::format( "VPClustersPerEvent{}", *( title_fs.begin() + i ) ),
                            fmt::format( "Frequency of # of VP clusters per event for {} ; Clusters/event;",
                                         *( title_fs.begin() + i ) ),
                            Axis1D{ 5001, -1, 10001 } );
        m_mnr_fs.emplace( i, std::nullopt );
        m_mnr_fs[i].emplace( this, fmt::format( "VPClustersPerModule{}", *( title_fs.begin() + i ) ),
                             fmt::format( "VP clusters per module for {}(Clusters vs Module); Clusters/event;",
                                          *( title_fs.begin() + i ) ),
                             Axis1D{ 5001, -1, 10001 } );
      }
      if ( debug ) {
        for ( unsigned int imod = 0; imod < VP::NModules / 2; imod++ ) {
          // Book for cluster map in A/C side;
          m_gxy_C.emplace( imod, std::nullopt );
          m_gxy_C[imod].emplace( this, fmt::format( "VPClusterMapGlobalXYOnMod{}", imod ),
                                 fmt::format( "VP cluster map for Module {} (y vs x); x (mm); y (mm)", imod ),
                                 Axis1D{ 800, -80, 80 }, Axis1D{ 600, -60, 60 } );

          m_gxy_A.emplace( imod, std::nullopt );
          m_gxy_A[imod].emplace( this, fmt::format( "VPClusterMapGlobalXYOnMod{}", imod + 1 ),
                                 fmt::format( "VP cluster map for Module {} (y vs x); x (mm); y (mm)", imod + 1 ),
                                 Axis1D{ 800, -80, 80 }, Axis1D{ 600, -60, 60 } );
        }

      } // End of debug

      for ( unsigned int iasic = 0; iasic < NAsics; iasic++ ) {
        m_ncla.emplace( iasic, std::nullopt );
        m_ncla[iasic].emplace(
            this, fmt::format( "VPClustersPerEventPerASICsOnASIC{}", iasic ),
            fmt::format( "Frequency of # of VP clusters per event for ASIC{}; Clusters/event", iasic ),
            Axis1D{ 50, 0, 50 } );
      }

    } // End of Flag
  } );

  // Finish the initialising
};

// ===============================================================
// Main
// ===============================================================
template <OutputMode outmode>
void LHCb::VPClusterMonitors<outmode>::operator()( const LHCb::ODIN&                       odin,
                                                   const std::vector<LHCb::VPFullCluster>& vpClusters,
                                                   const zSensors&                         zSensors ) const {

  // Get event information from ODIN
  auto bxid{ odin.bunchId() };
  auto event_type{ odin.eventType() };
  auto TAE_index = 10 - odin.timeAlignmentEventIndex() - 2;

  const auto runNr{ odin.runNumber() };
  const auto evtNr{ odin.eventNumber() };
  const auto bxtype{ odin.bunchCrossingType() };

  using BXTypes_odin = LHCb::ODINImplementation::v7::ODIN::BXTypes;

  FillScheme FS{ None };
  bool       gotFS = true;

  try {
    if ( bxtype == BXTypes_odin::BeamCrossing ) {
      FS = BB;

      if ( event_type & static_cast<std::uint16_t>( 1 << 8 ) ) { FS = IsolatedBB; }
    } else if ( bxtype == BXTypes_odin::NoBeam ) {

      FS = EE;
      // BxTypes of three bxids before and after this one
      // We have no way to identify the isolated EE from ODIN for now

      // Check if spillover/prespill EE (shouldn't be in any filling schemes but good to check)
      if ( ( event_type & static_cast<std::uint16_t>( 1 << 11 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 12 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 13 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 14 ) ) ) {
        if ( TAE_index == 3 ) FS = EESpillover0; // BB -> *EE*
        if ( TAE_index == 2 ) FS = EESpillover1; // BB -> not BB -> *EE*
        if ( TAE_index == 1 ) FS = EESpillover2; // BB -> not BB -> not BB -> *EE*
        if ( TAE_index == 5 ) FS = EEPrespill0;  // *EE* -> BB
        if ( TAE_index == 6 ) FS = EEPrespill1;  // *EE* -> not BB -> BB
        if ( TAE_index == 7 ) FS = EEPrespill2;  // *EE* -> not BB -> not BB -> BB
      }

    } else if ( bxtype == BXTypes_odin::Beam1 ) {
      FS = BE;

    } else if ( bxtype == BXTypes_odin::Beam2 ) {
      FS = EB;
    }
  } catch ( const std::runtime_error& e ) { gotFS = false; }
  if ( debug ) {
    info() << "BxID defined: " << bxid << " txt BxType: " << bxtype << " FS type: " << static_cast<int>( FS )
           << " ODIN event type: " << event_type << " TAE index:  " << TAE_index << endmsg;
  }
  // Fill total number of clusters
  if constexpr ( outmode == OutputMode::Histograms ) {

    int nclPerEvent = vpClusters.size();

    if ( flag_EE ) ( *m_ncl )[nclPerEvent] += 1;
    if ( flag_EE ) ( *m_ncld )[nclPerEvent] += 1;

    if ( gotFS ) {
      // Fill all filling scheme related plots for double checking

      switch ( FS ) {
      case None:
        break;
      case BB:
        if ( flag_EE ) ( *m_fs )[{ bxid, 0 }] += 1;

        break;
      case IsolatedBB:
        if ( flag_EE ) ( *m_fs )[{ bxid, 1 }] += 1;

        break;
      case EESpillover0:
        if ( flag_EE ) ( *m_fs )[{ bxid, 2 }] += 1;

        break;
      case EESpillover1:
        if ( flag_EE ) ( *m_fs )[{ bxid, 2 }] += 1;

        break;
      case EESpillover2:
        if ( flag_EE ) ( *m_fs )[{ bxid, 2 }] += 1;

        break;
      case EE:
        if ( flag_EE ) ( *m_fs )[{ bxid, 3 }] += 1;
        break;

      case EEPrespill0:
        if ( flag_EE ) ( *m_fs )[{ bxid, 5 }] += 1;

        break;
      case EEPrespill1:
        if ( flag_EE ) ( *m_fs )[{ bxid, 5 }] += 1;

        break;
      case EEPrespill2:
        if ( flag_EE ) ( *m_fs )[{ bxid, 5 }] += 1;

        break;
      case BE:
        if ( flag_EE ) ( *m_fs )[{ bxid, 6 }] += 1;
        break;
      case EB:
        if ( flag_EE ) ( *m_fs )[{ bxid, 7 }] += 1;
        break;
      case MaxFS:
        break;
      }

      if ( FS != FillScheme::None && FS != FillScheme::MaxFS ) {
        if ( flag_EE ) ( *m_fsncl[static_cast<uint>( FS ) - 1] )[nclPerEvent] += 1; // first FS is 1 not 0
      }
    }
  }

  // Initialise variables for sorting clusters (for m_ncla)
  std::vector<std::pair<int, int>> combined_chip_sen; // first: chip, second: sen;
  // reserve space!
  combined_chip_sen.reserve( vpClusters.size() );

  // Loop over clusters vector
  for ( const auto& vpCluster : vpClusters ) {

    // Get VP Channel IDs of pixels in cluster
    const LHCb::Detector::VPChannelID vpID  = vpCluster.channelID();
    const auto                        vpIDs = vpCluster.pixels();

    // Get cluster size
    uint nPixels = vpIDs.size();

    // Get cluster locations
    const auto mod    = vpID.module();
    const auto chipNr = to_unsigned( vpID.chip() );
    const auto sen    = to_unsigned( vpID.sensor() );
    const auto senNr  = get_sensorNr( sen );
    const auto asic   = get_asic( sen, chipNr );
    const auto asicNr = get_asicNr( senNr, chipNr );
    const auto row    = to_unsigned( vpID.row() );
    const auto col    = to_unsigned( vpID.col() );
    const auto x      = vpCluster.x();
    const auto y      = vpCluster.y();
    const auto z      = vpCluster.z();
    const auto xfrac  = vpCluster.xfraction();
    const auto yfrac  = vpCluster.yfraction();
    const auto rad    = sqrt( pow( x, 2 ) + pow( y, 2 ) );

    combined_chip_sen.emplace_back( asicNr, sen );

    if constexpr ( outmode == OutputMode::Histograms ) {

      // Fill objects
      if ( flag_EE ) ( *m_mnr )[mod] += 1;
      if ( flag_EE ) ( *m_mz )[zSensors.z( sen )] += 1;
      if ( flag_EE ) ( *m_ch )[chipNr] += 1;
      if ( flag_EE ) ( *m_se )[sen] += 1;
      if ( flag_EE ) ( *m_senr )[senNr] += 1;
      if ( flag_EE ) ( *m_as )[asic] += 1;
      if ( flag_EE ) ( *m_asnr )[asicNr] += 1;

      if ( gotFS ) {
        if ( FS != FillScheme::None && FS != FillScheme::MaxFS ) {
          if ( flag_EE ) ( *m_mnr_fs[static_cast<uint>( FS ) - 1] )[mod] += 1; // first FS is 1 not 0
        }
      }

      // Fill radius and global x,y plots
      if ( flag_EE ) ( *m_rad )[rad] += 1;
      if ( flag_EE ) ( *m_radz )[{ zSensors.z( sen ), rad }] += 1;
      const uint station = mod / 2;
      if ( mod % 2 == 0 ) {

        if ( flag_EE ) ( *m_gxy_allC )[{ x, y }] += 1;
        if ( debug ) {
          if ( flag_EE ) ( *m_gxy_C[station] )[{ x, y }] += 1;
        }

      } else {
        if ( flag_EE ) ( *m_gxy_allA )[{ x, y }] += 1;
        if ( debug ) {
          if ( flag_EE ) ( *m_gxy_A[station] )[{ x, y }] += 1;
        }
      }

      // Fill the 2D cluster vs. sensor map using individual pixel in one cluster
      for ( const auto& pID : vpIDs ) {
        const auto chipNr_p = to_unsigned( pID.chip() );
        const auto row_p    = to_unsigned( pID.row() );
        const auto col_p    = to_unsigned( pID.col() );

        if ( m_allASICs ) { ( *m_sp[sen] )[{ col_p + ( VP::NRows * chipNr_p ), row_p }] += 1.; }
      }

      // Fill cluster size histograms
      if ( flag_EE ) ( *m_pcl )[nPixels] += 1;
      if ( flag_EE ) ( *m_pmnr )[{ mod, nPixels }] += 1;
      if ( flag_EE ) ( *m_pmz )[{ zSensors.z( sen ), nPixels }] += 1;
      if ( flag_EE ) ( *m_pse )[{ sen, nPixels }] += 1;
      if ( flag_EE ) ( *m_psenr )[{ senNr, nPixels }] += 1;
      if ( flag_EE ) ( *m_pas )[{ asic, nPixels }] += 1;
      if ( flag_EE ) ( *m_pasnr )[{ asicNr, nPixels }] += 1;

    } else {

      Tuple clusTuple = nTuple( "VPClusterMonitorsTuple", "" );

      clusTuple->column( "BxID", bxid ).ignore();
      clusTuple->column( "BXType", static_cast<int>( bxtype ) ).ignore();
      clusTuple->column( "FillScheme", static_cast<int>( FS ) ).ignore();
      clusTuple->column( "EventNr", evtNr ).ignore();
      clusTuple->column( "RunNr", runNr ).ignore();
      clusTuple->column( "Module", mod ).ignore();
      clusTuple->column( "ChipNr", chipNr ).ignore();
      clusTuple->column( "Sensor", sen ).ignore();
      clusTuple->column( "SensorNr", senNr ).ignore();
      clusTuple->column( "ASIC", asic ).ignore();
      clusTuple->column( "ASICNr", asicNr ).ignore();
      clusTuple->column( "Row", row ).ignore();
      clusTuple->column( "Column", col ).ignore();
      clusTuple->column( "x", x ).ignore();
      clusTuple->column( "y", y ).ignore();
      clusTuple->column( "z", z ).ignore();
      clusTuple->column( "xfraction", xfrac ).ignore();
      clusTuple->column( "yfraction", yfrac ).ignore();
      clusTuple->column( "Radius", rad ).ignore();
      clusTuple->column( "ClusterSize", nPixels ).ignore();
      clusTuple->write().ignore();
    }

  } // Loop over clusters per event

  int nclPerEvent = vpClusters.size();

  // Sort the vector of pairs using the default comparison function
  std::sort( combined_chip_sen.begin(), combined_chip_sen.end() );

  int chipClust = 1;
  for ( int i = 0; i < nclPerEvent; i++ ) {
    // Stop when reaching the end of a cluster set (next 'if' will fail otherwise)
    if ( i == nclPerEvent - 1 ) {
      if ( flag_EE ) ( *m_ncla[combined_chip_sen[i].first] )[chipClust] += 1;
      break;
    }

    // If there is more than one cluster in the same chip (identical next values in the sorted array) increase the value
    // to be added
    if ( combined_chip_sen[i].first == combined_chip_sen[i + 1].first &&
         combined_chip_sen[i].second == combined_chip_sen[i + 1].second ) {
      chipClust += 1;
      continue;
    }
    // Fill and reset
    if ( flag_EE ) ( *m_ncla[combined_chip_sen[i].first] )[chipClust] += 1;
    chipClust = 1;
  } // Loop to fill the m_ncla histogram

} //

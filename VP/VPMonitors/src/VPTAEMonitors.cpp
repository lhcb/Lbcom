/*****************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#include "Event/RecVertex.h"

#include "Event/ODIN.h"
#include "Event/VPFullCluster.h"
#include "Kernel/VPConstants.h"

#include "VPDet/DeVP.h"

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "VPDet/DeVPSensor.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"

#include <unistd.h>

#include "Gaudi/Accumulators/Histogram.h"
#include "Gaudi/Accumulators/HistogramArray.h"

#include <string>
#include <vector>

/* @class  VPTAEMonitors.cpp VPTAEMonitors.cpp
 *
 * * Class for plotting:
 *   - used for TAE plots (True/Fake)
 *
 *
 * * Notes:
 *   - Currently use Odin features to obtain the FillingScheme
 * @author Lanxing Li
 * @date   2023-10-04
 */

namespace {

  // Adjust bxid when it is shifted for some operation, so that it is always 1-3564
  // This only works if the operation shifts the bxid by at most 3564

  constexpr uint get_asic( uint sensor, uint chipNr ) { return 3 * sensor + chipNr; }
  // Get asic in range [0, 623]

  constexpr uint get_sensorNr( uint sensor ) { return sensor % VP::NSensorsPerModule; }
  // Transform sensor number to range [0,3] for plotting purposes

  constexpr uint get_asicNr( uint sensorNr, uint chipNr ) { return 3 * sensorNr + chipNr; }
  // Transform ASIC number to range [0,11] for plotting purposes
} // anonymous namespace

// Enumerators
enum struct OutputMode { Histograms, Tuples };

namespace LHCb {
  template <OutputMode outmode>
  class VPTAEMonitors : public Algorithm::Consumer<void( const ODIN&, const std::vector<VPFullCluster>& )> {

  public:
    VPTAEMonitors( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;

    void operator()( const ODIN&, const std::vector<VPFullCluster>& ) const override;
    bool is_trueTAE = false;

    const int m_maxTaeIndex = 10;

  private:
    Gaudi::Property<bool> debug{ this, "DebugMode", false, "Whether to produce extra plots for debugging" };

    // Histogram objects to book
    mutable Gaudi::Accumulators::Histogram<2> m_fakeTAE{
        this,
        "VPClustersPerEventFakeTAEAny",
        "EE -> EE -> EE -> Beam-Beams -> EE -> EE -> EE ;  ; Clusters/event",
        { 9, -0.5, 8.5, "X" },
        { 5001, -1, 10001, "Y" } };
    // Number of clusters per event for prespill -2, -1, 0, BBs, spillover +0, +1 +2 (EE)

    mutable Gaudi::Accumulators::Histogram<2> m_fakeTAE_lead{ this,
                                                              "VPClustersPerEventFakeTAE_lead",
                                                              "EE -> EE -> EE -> Beam-Beams(lead);  ; Clusters/event",
                                                              { 9, -0.5, 8.5, "X" },
                                                              { 5001, -1, 10001, "Y" } };
    // Number of clusters per event for prespill -2, -1, 0, BB in lead, spillover +0, +1 +2 (EE)

    mutable Gaudi::Accumulators::Histogram<2> m_fakeTAE_trail{
        this,
        "VPClustersPerEventFakeTAE_trail",
        "Beam-Beams(trail) -> EE -> EE -> EE ;  ; Clusters/event",
        { 9, -0.5, 8.5, "X" },
        { 5001, -1, 10001, "Y" } };
    // Number of clusters per event for BB at tail, spillver 0, -1, -2 (EE)

    mutable Gaudi::Accumulators::Histogram<2> m_fakeTAE_lead_trail{
        this,
        "VPClustersPerEventFakeTAE_lead_trail",
        "EE -> EE -> EE -> Beam-Beams(lead) -> Beam-Beams(trail) -> EE -> EE -> EE ;  ; "
        "Clusters/event",
        { 9, -0.5, 8.5, "X" },
        { 5001, -1, 10001, "Y" } };
    // Number of clusters per event for prespill -2, -1, 0, BB at lead, BB at tail, spillver 0, +1, +2 (EE)

    mutable Gaudi::Accumulators::Histogram<2> m_trueTAE{
        this,
        "VPClustersPerEventTrueTAE",
        "EE -> EE -> EE -> iso Beam-Beam -> EE -> EE -> EE ;  ; Clusters/event",
        { 9, -0.5, 8.5, "X" },
        { 5001, -1, 10001, "Y" } };
    // Number of clusters per event for prespill -2, -1, 0, isolated BB, spillover +0, +1 +2 (EE)

    mutable Gaudi::Accumulators::Histogram<2> m_TAE_all{
        this,
        "VPClustersPerEvent_TAE_all",
        "EE -> EE -> EE -> Beam-Beams -> EE -> EE -> EE ;  ; Clusters/event",
        { 9, -0.5, 8.5, "X" },
        { 5001, -1, 10001, "Y" } };
    // Number of clusters per event for prespill -2, -1, 0, BBs (isolated + lead + trail),  spillover +0, +1 +2 (EE)

    mutable Gaudi::Accumulators::Histogram<2> m_trueTAE_2D_BB{ this,
                                                               "true_TAE_map",
                                                               "True TAE per ASIC; Module ; ASIC Number",
                                                               { VP::NModules, -0.5, VP::NModules - 0.5, "X" },
                                                               { 12, -0.5, 11.5, "Y" } };

    mutable Gaudi::Accumulators::Histogram<2> m_trueTAE_2D_SP1{ this,
                                                                "true_TAE_map_SP1",
                                                                "True TAE per ASIC; Module ; ASIC Number",
                                                                { VP::NModules, -0.5, VP::NModules - 0.5, "X" },
                                                                { 12, -0.5, 11.5, "Y" } };

    mutable Gaudi::Accumulators::Histogram<2> m_trueTAE_2D_PS1{ this,
                                                                "true_TAE_map_PS1",
                                                                "True TAE per ASIC; Module ; ASIC Number",
                                                                { VP::NModules, -0.5, VP::NModules - 0.5, "X" },
                                                                { 12, -0.5, 11.5, "Y" } };

    mutable Gaudi::Accumulators::Histogram<2> m_fakeTAE_2D_BB{ this,
                                                               "fake_TAE_map",
                                                               "Fake TAE per ASIC; Module ; ASIC Number",
                                                               { VP::NModules, -0.5, VP::NModules - 0.5, "X" },
                                                               { 12, -0.5, 11.5, "Y" } };

    mutable Gaudi::Accumulators::Histogram<2> m_fakeTAE_2D_SP1{ this,
                                                                "fake_TAE_map_SP1",
                                                                "Fake TAE per ASIC; Module ; ASIC Number",
                                                                { VP::NModules, -0.5, VP::NModules - 0.5, "X" },
                                                                { 12, -0.5, 11.5, "Y" } };

    mutable Gaudi::Accumulators::Histogram<2> m_fakeTAE_2D_PS1{ this,
                                                                "fake_TAE_map_PS1",
                                                                "Fake TAE per ASIC; Module ; ASIC Number",
                                                                { VP::NModules, -0.5, VP::NModules - 0.5, "X" },
                                                                { 12, -0.5, 11.5, "Y" } };

    mutable Gaudi::Accumulators::Histogram<2> m_TAE_counter_true{ this,
                                                                  "TAE_counter_true",
                                                                  "Events counter for true TAE; ASIC ; Bunch Type",
                                                                  { 624, -0.5, 624 - 0.5, "X" },
                                                                  { 9, -0.5, 8.5, "Y" } };

    mutable Gaudi::Accumulators::Histogram<2> m_TAE_counter_fake{ this,
                                                                  "TAE_counter_fake",
                                                                  "True TAE per ASIC; Module ; ASIC Number",
                                                                  { 624, -0.5, 624 - 0.5, "X" },
                                                                  { 9, -0.5, 8.5, "Y" } };

    struct optionalHistos {
      mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<2>, 624> m_trueTAE_asic;
      mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<2>, 624> m_fakeTAE_asic;

      optionalHistos( const VPTAEMonitors* owner )
          : m_trueTAE_asic{ owner,
                            []( int n ) { return fmt::format( "true_TAE_plot_for_ASIC_{}", n ); },
                            []( int n ) { return fmt::format( "True TAE plot for ASIC {}; ; Clusters/BxID", n ); },
                            { 9, -4.5, 4.5 },
                            { 100, -0.5, 99.5 } }
          , m_fakeTAE_asic{ owner,
                            []( int n ) { return fmt::format( "fake_TAE_plot_for_ASIC_{}", n ); },
                            []( int n ) { return fmt::format( "Fake TAE plot for ASIC {}; ; Clusters/BxID", n ); },
                            { 9, -4.5, 4.5 },
                            { 100, -0.5, 99.5 } } {}
    };

    std::unique_ptr<optionalHistos> optional_histos;

  }; // end of class
} // namespace LHCb
// Declaration of the Algorithm Factory
using VPTAEMonitorsHistograms = LHCb::VPTAEMonitors<OutputMode::Histograms>;
using VPTAEMonitorsTuples     = LHCb::VPTAEMonitors<OutputMode::Tuples>;
DECLARE_COMPONENT_WITH_ID( VPTAEMonitorsHistograms, "VPTAEMonitors" )
DECLARE_COMPONENT_WITH_ID( VPTAEMonitorsTuples, "VPTAEMonitorsNT" )

// ===============================================================
// Constructor
// ===============================================================
template <OutputMode outmode>
LHCb::VPTAEMonitors<outmode>::VPTAEMonitors( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {
                    KeyValue{ "ODINLocation", LHCb::ODINLocation::Default },
                    KeyValue{ "ClusterLocation", LHCb::VPFullClusterLocation::Default },
                } ) {}
// ===============================================================
// // Initialize
// // ===============================================================
template <OutputMode outmode>
StatusCode LHCb::VPTAEMonitors<outmode>::initialize() {
  return Consumer::initialize().andThen( [&]() {
    if ( debug ) { optional_histos = std::make_unique<optionalHistos>( this ); }
  } );
};

// ===============================================================
// Main
// ===============================================================
template <OutputMode outmode>
void LHCb::VPTAEMonitors<outmode>::operator()( const LHCb::ODIN&                       odin,
                                               const std::vector<LHCb::VPFullCluster>& vpClusters ) const {

  // Get event information from ODIN
  auto event_type{ odin.eventType() };
  auto TAE_index = m_maxTaeIndex - odin.timeAlignmentEventIndex() - 2;

  std::vector<int> asic_loc; // find asic No.
  asic_loc.reserve( vpClusters.size() );

  for ( const auto& vpCluster : vpClusters ) {
    const LHCb::Detector::VPChannelID vpID   = vpCluster.channelID();
    const auto                        vpIDs  = vpCluster.pixels();
    const auto                        mod    = vpID.module();
    const auto                        chipNr = to_unsigned( vpID.chip() );
    const auto                        sen    = to_unsigned( vpID.sensor() );
    const auto                        asic   = get_asic( sen, chipNr );
    const auto                        senNr  = get_sensorNr( sen );
    const auto                        asicNr = get_asicNr( senNr, chipNr );

    asic_loc.push_back( asic );

    if ( TAE_index < m_maxTaeIndex && odin.timeAlignmentEventIndex() != 0 ) {
      if ( ( event_type & static_cast<std::uint16_t>( 1 << 8 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 11 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 12 ) ) ) {
        if ( TAE_index == 4 ) ++m_trueTAE_2D_BB[{ mod, asicNr }];
        if ( TAE_index == 3 ) ++m_trueTAE_2D_PS1[{ mod, asicNr }];
        if ( TAE_index == 5 ) ++m_trueTAE_2D_SP1[{ mod, asicNr }];
      }
      if ( ( ( event_type & static_cast<std::uint16_t>( 1 << 8 ) ) == 0 ) &&
           ( ( event_type & static_cast<std::uint16_t>( 1 << 9 ) ) ||
             ( event_type & static_cast<std::uint16_t>( 1 << 13 ) ) ) ) {
        if ( TAE_index == 4 ) ++m_fakeTAE_2D_BB[{ mod, asicNr }];
        if ( TAE_index == 3 ) ++m_fakeTAE_2D_PS1[{ mod, asicNr }];
        if ( TAE_index == 5 ) ++m_fakeTAE_2D_SP1[{ mod, asicNr }];
      }
      if ( ( ( event_type & static_cast<std::uint16_t>( 1 << 8 ) ) == 0 ) &&
           ( ( event_type & static_cast<std::uint16_t>( 1 << 10 ) ) ||
             ( event_type & static_cast<std::uint16_t>( 1 << 14 ) ) ) ) {
        if ( TAE_index == 4 ) ++m_fakeTAE_2D_BB[{ mod, asicNr }];
        if ( TAE_index == 3 ) ++m_fakeTAE_2D_PS1[{ mod, asicNr }];
        if ( TAE_index == 5 ) ++m_fakeTAE_2D_SP1[{ mod, asicNr }];
      }
    }
  }
  std::sort( asic_loc.begin(), asic_loc.end() );

  if ( outmode == OutputMode::Histograms ) {
    int nclPerEvent = vpClusters.size();

    if ( TAE_index < m_maxTaeIndex && odin.timeAlignmentEventIndex() != 0 ) {
      if ( ( event_type & static_cast<std::uint16_t>( 1 << 8 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 11 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 12 ) ) ) {
        ++m_trueTAE[{ TAE_index, nclPerEvent }];
        ++m_TAE_all[{ TAE_index, nclPerEvent }];
      }
      if ( ( event_type & static_cast<std::uint16_t>( 1 << 9 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 13 ) ) ) {
        ++m_fakeTAE[{ TAE_index, nclPerEvent }];
        ++m_fakeTAE_lead[{ TAE_index, nclPerEvent }];
        ++m_fakeTAE_lead_trail[{ TAE_index, nclPerEvent }];
        ++m_TAE_all[{ TAE_index, nclPerEvent }];
      }
      if ( ( event_type & static_cast<std::uint16_t>( 1 << 10 ) ) ||
           ( event_type & static_cast<std::uint16_t>( 1 << 14 ) ) ) {
        ++m_fakeTAE[{ TAE_index, nclPerEvent }];
        ++m_fakeTAE_trail[{ TAE_index, nclPerEvent }];
        ++m_fakeTAE_lead_trail[{ TAE_index, nclPerEvent }];
        ++m_TAE_all[{ TAE_index, nclPerEvent }];
      }
    }
    // Add the TAE per ASIC (debug mode only)

    int chipClust = 1;
    for ( int i = 0; i < nclPerEvent; i++ ) {

      if ( i == nclPerEvent - 1 ) {

        if ( TAE_index < m_maxTaeIndex && odin.timeAlignmentEventIndex() != 0 ) {
          if ( ( event_type & static_cast<std::uint16_t>( 1 << 8 ) ) ||
               ( event_type & static_cast<std::uint16_t>( 1 << 11 ) ) ||
               ( event_type & static_cast<std::uint16_t>( 1 << 12 ) ) ) {
            if ( debug ) ++optional_histos->m_trueTAE_asic[asic_loc[i]][{ TAE_index, chipClust }];
            if ( chipClust == 1 ) ++m_TAE_counter_true[{ asic_loc[i], TAE_index }];
          }
          if ( ( event_type & static_cast<std::uint16_t>( 1 << 9 ) ) ||
               ( event_type & static_cast<std::uint16_t>( 1 << 13 ) ) ) {
            if ( debug ) ++optional_histos->m_fakeTAE_asic[asic_loc[i]][{ TAE_index, chipClust }];
            if ( chipClust == 1 ) ++m_TAE_counter_fake[{ asic_loc[i], TAE_index }];
          }
          if ( ( event_type & static_cast<std::uint16_t>( 1 << 10 ) ) ||
               ( event_type & static_cast<std::uint16_t>( 1 << 14 ) ) ) {
            if ( debug ) ++optional_histos->m_fakeTAE_asic[asic_loc[i]][{ TAE_index, chipClust }];
            if ( chipClust == 1 ) ++m_TAE_counter_fake[{ asic_loc[i], TAE_index }];
          }
        }
        break;
      }
      if ( asic_loc[i] == asic_loc[i + 1] ) {
        chipClust += 1;
        continue;
      }
      // Fill and reset
      if ( TAE_index < m_maxTaeIndex && odin.timeAlignmentEventIndex() != 0 ) {
        if ( ( event_type & static_cast<std::uint16_t>( 1 << 8 ) ) ||
             ( event_type & static_cast<std::uint16_t>( 1 << 11 ) ) ||
             ( event_type & static_cast<std::uint16_t>( 1 << 12 ) ) ) {
          if ( debug ) ++optional_histos->m_trueTAE_asic[asic_loc[i]][{ TAE_index, chipClust }];
          ++m_TAE_counter_true[{ asic_loc[i], TAE_index }];
        }
        if ( ( event_type & static_cast<std::uint16_t>( 1 << 9 ) ) ||
             ( event_type & static_cast<std::uint16_t>( 1 << 13 ) ) ) {
          if ( debug ) ++optional_histos->m_fakeTAE_asic[asic_loc[i]][{ TAE_index, chipClust }];
          ++m_TAE_counter_fake[{ asic_loc[i], TAE_index }];
        }
        if ( ( event_type & static_cast<std::uint16_t>( 1 << 10 ) ) ||
             ( event_type & static_cast<std::uint16_t>( 1 << 14 ) ) ) {
          if ( debug ) ++optional_histos->m_fakeTAE_asic[asic_loc[i]][{ TAE_index, chipClust }];
          ++m_TAE_counter_fake[{ asic_loc[i], TAE_index }];
        }
      }
      chipClust = 1;
    }
  }

} // operator()

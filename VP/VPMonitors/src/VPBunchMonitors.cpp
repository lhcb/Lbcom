/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/ODIN.h"
#include "Event/VPFullCluster.h"
#include "Kernel/VPConstants.h"

#include "VPDet/DeVP.h"

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "VPDet/DeVPSensor.h"

#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators/Histogram.h"
#include "Gaudi/Accumulators/HistogramArray.h"

#include <algorithm>
#include <cmath>
#include <string>
#include <utility>
#include <vector>

/* @class VPBunchMonitors VPBunchMonitors.cpp
 *
 * * Class for plotting
 *   - Number of clusters per bunch ID for each module
 *
 * @author T.H.McGrath
 * @date   2022-05-19
 */

namespace {

  constexpr auto NAsicsTotal = VP::NSensorsPerModule * VP::NChipsPerSensor * VP::NModules;

  // Get asic in range [0, 623]
  constexpr uint get_asic( uint sensor, uint chipNr ) { return 3 * sensor + chipNr; }

} // namespace

namespace LHCb {
  class VPBunchMonitors : public Algorithm::Consumer<void( const ODIN&, const std::vector<VPFullCluster>& )> {

  public:
    VPBunchMonitors( const std::string& name, ISvcLocator* pSvcLocator );

    void operator()( const ODIN&, const std::vector<VPFullCluster>& ) const override;

  private:
    // Input variables
    Gaudi::Property<bool> applyCut{ this, "ClearNoise", false, "Whether to remove noisy pixels to clean data" };
    Gaudi::Property<bool> printInfo{ this, "PrintInfo", true, "Whether to print BxID info for each event" };
    Gaudi::Property<bool> debug{ this, "DebugMode", true, "Whether to produce extra plots for debugging" };

    // Book all plots
    mutable Gaudi::Accumulators::Histogram<1> m_ncl{
        this, // Clusters / event frequency
        "VPClustersPerEvent",
        "Frequency of # of VP clusters per event (Frequency vs Clusters/event); Clusters/event;",
        { 25, 0., 25. } };
    mutable Gaudi::Accumulators::Histogram<1> m_bxid{
        this, "BxIDFrequency", "# of VP clusters per BxID (Frequency vs BxID); BxID;", { 3564, 0.5, 3564.5 } };
    mutable Gaudi::Accumulators::Histogram<2> m_bxidpe{
        this,
        "VPClustersPerEventVSBxID",
        "# of VP clusters per event per BxID (Clusters/event vs BxID); BxID; Clusters/event",
        { 3564, 0.5, 3564.5 },
        { 5001, -1, 10001 } };
    mutable Gaudi::Accumulators::Histogram<2> m_allmods{
        this,
        "VPClustersPerModulePerBxID",
        "Cluster frequency per module per BxID (BxID vs Modules); Module; BxId",
        { VP::NModules, -0.5, VP::NModules - 0.5 },
        { 3564, 0.5, 3564.5 } };
    mutable Gaudi::Accumulators::Histogram<2> m_allasics{
        this,
        "VPClustersPerASICPerBxID",
        "Cluster frequency per ASIC per BxID (BxID vs ASIC); ASIC; BxId",
        { NAsicsTotal, -0.5, NAsicsTotal - 0.5 },
        { 3564, 0.5, 3564.5 } };

    // Only book per module plots if in debug mode (currently add all debug plots)
    mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<2>, VP::NModules> m_bxidpepm{
        this,
        "VPClustersPerEventVSBxIDForModule_{}",
        "# of VP clusters per event per BxID for Module {}  (Clusters/event vs BxID); BxID; Clusters/event",
        { 3564, 0.5, 3564.5, "X" },
        { 25, 0., 25., "Y" } };

    mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<1>, VP::NModules> m_nclpm{
        this,
        "VPClustersPerEventForModule_{}",
        "Frequency of # of VP clusters per event for Module {} (Frequency vs Clusters/event); Clusters/event;",
        { 25, 0., 25., "X" } };

    mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<1>, VP::NModules> m_listmods{
        this,
        "VPClustersPerBxIDForModule_{}",
        "Cluster frequency per BxID for Module {}  (Clusters vs BxID); BxID; Clusters",
        { 3564, 0.5, 3564.5, "X" } };

    mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<2>, VP::NModules> m_pixmaps{
        this,
        "VPClusterssMapModule_{}",
        "VP cluster map on module {} (Row vs Column); Column; Row",
        { VP::NColumns, 0, VP::NColumns, "X" },
        { VP::NRows, 0, VP::NRows, "Y" } };

    mutable Gaudi::Accumulators::Histogram<1> m_csize{
        this,
        "VPClusterSize",
        "Number of pixels per VP cluster (Frequency vs Cluster Size); Cluster Size;",
        { 49, 1, 50 } };
  }; // End of class
} // namespace LHCb
// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( LHCb::VPBunchMonitors, "VPBunchMonitors" )

// ===============================================================
// Constructor
// ===============================================================
LHCb::VPBunchMonitors::VPBunchMonitors( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "ODINLocation", LHCb::ODINLocation::Default },
                  KeyValue{ "ClusterLocation", LHCb::VPFullClusterLocation::Default } } ) {}

// ===============================================================
// Main
// ===============================================================
void LHCb::VPBunchMonitors::operator()( const LHCb::ODIN&                       odin,
                                        const std::vector<LHCb::VPFullCluster>& vpClusters ) const {

  // Get event information from ODIN
  const auto bxid  = odin.bunchId();
  const auto runNr = odin.runNumber();
  const auto evtNr = odin.eventNumber();

  if ( printInfo ) {
    info() << "BxID: " << bxid << " run #: " << runNr << " event #: " << evtNr << endmsg;
    info() << "Number of clusters: " << vpClusters.size() << endmsg;
  }

  std::vector<uint> nClusPerMod( VP::NModules, 0 );
  std::vector<uint> nClusPerChip( NAsicsTotal, 0 );
  uint              nClusPerEvent( 0 );

  // Loop over clusters vector
  for ( const auto& vpCluster : vpClusters ) {

    // Get VP Channel IDs of pixels in cluster
    const auto vpID   = vpCluster.channelID();
    const auto pixels = vpCluster.pixels();

    // Get module where cluster is
    const auto mod  = vpID.module();
    const auto asic = get_asic( to_unsigned( vpID.sensor() ), to_unsigned( vpID.chip() ) );
    // info() << "Module: " << mod << endmsg;

    // Get pixel
    const auto row = to_unsigned( vpID.row() );
    const auto col = to_unsigned( vpID.col() );

    // Fill histograms
    ++m_bxid[bxid];
    ++m_allmods[{ mod, bxid }];
    ++m_allasics[{ asic, bxid }];
    ++m_csize[(double)pixels.size()];

    // Add each cluster from asic
    ++nClusPerChip[asic];
    ++nClusPerEvent;

    if ( debug ) {

      ++m_listmods[mod][bxid];
      ++m_pixmaps[mod][{ col, row }];
      ++nClusPerMod[mod];
    }

  } // Loop over clusters per event

  ++m_bxidpe[{ bxid, nClusPerEvent }];
  ++m_ncl[(double)nClusPerEvent];

  if ( debug ) {

    for ( uint modNr = 0; modNr < VP::NModules; ++modNr ) {
      ++m_nclpm[modNr][nClusPerMod[modNr]];
      ++m_bxidpepm[modNr][{ bxid, nClusPerMod[modNr] }];
    }
  }

  // clear vector before next event
  nClusPerMod.clear();
}

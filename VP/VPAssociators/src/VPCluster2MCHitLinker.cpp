/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/VPCluster.h"
#include "Event/VPDigit.h"
#include <Associators/Location.h>

namespace LHCb::VP {

  /**
   * These algorithms create association tables between the VP clusters
   * and the corresponding MC hits, based on the association tables for
   * individual pixels produced by VPDigitLinker.
   */
  class Cluster2MCHitLinker : public Algorithm::Linker<LinksByKey( const VPClusters&, const LinksByKey& )> {
  public:
    // Standard constructor
    Cluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator );

    LinksByKey operator()( const VPClusters& clusters,
                           const LinksByKey& digitLinks ) const override; // < Algorithm execution
  };

  DECLARE_COMPONENT_WITH_ID( Cluster2MCHitLinker, "VPCluster2MCHitLinker" )

} // namespace LHCb::VP

LHCb::VP::Cluster2MCHitLinker::Cluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : Linker( name, pSvcLocator,
              { KeyValue{ "ClusterLocation", VPClusterLocation::Default },
                KeyValue{ "VPDigit2MCHitLinksLocation", Links::location( VPDigitLocation::Default + "2MCHits" ) } },
              KeyValue{ "OutputLocation", Links::location( VPClusterLocation::Default + "2MCHits" ) } ) {}

LHCb::LinksByKey LHCb::VP::Cluster2MCHitLinker::operator()( const LHCb::VPClusters& clusters,
                                                            const LHCb::LinksByKey& bareDigitLinks ) const {
  // Get the association table between digits and hits.
  auto digitLinks = inputLinks<VPDigit, MCHit>( bareDigitLinks );

  // Create an association table between hits and clusters.
  auto output = outputLinks<MCHit>();
  // Loop over the clusters.

  for ( const VPCluster* cluster : clusters ) {
    std::map<const MCHit*, double> hitMap;
    double                         sum = 0.;
    // Loop over all pixels in a given cluster
    for ( const auto& pixel : cluster->pixels() ) {
      // Get the MCHit pointer from the associator
      const auto& range = digitLinks.from( pixel );
      for ( const auto& entry : range ) {
        const double weight = entry.weight();
        hitMap[entry.to()] += weight;
        sum += weight;
      }
      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Got " << range.size() << " MCHits from associator." << endmsg; }
    }
    if ( sum < 1.e-2 ) continue;
    for ( auto [hit, weight] : hitMap ) output.link( cluster, hit, weight / sum );
  }
  return output;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/VPDigit.h"
#include "Event/VPFullCluster.h"
#include <Associators/Location.h>

namespace LHCb::VP {

  class FullCluster2MCHitLinker : public Algorithm::Linker<LinksByKey( const std::vector<VPFullCluster>&,
                                                                       const MCHits& mcHits, const LinksByKey& )> {
  public:
    // Standard constructor
    FullCluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator );

    LinksByKey operator()( const std::vector<VPFullCluster>& clusters, const MCHits&,
                           const LinksByKey& ) const override; // < Algorithm execution
  };

  DECLARE_COMPONENT_WITH_ID( FullCluster2MCHitLinker, "VPFullCluster2MCHitLinker" )

} // namespace LHCb::VP

LHCb::VP::FullCluster2MCHitLinker::FullCluster2MCHitLinker( const std::string& name, ISvcLocator* pSvcLocator )
    : Linker( name, pSvcLocator,
              { KeyValue{ "ClusterLocation", VPFullClusterLocation::Default },
                KeyValue{ "MCHitLocation", MCHitLocation::VP },
                KeyValue{ "VPDigit2MCHitLinksLocation", Links::location( VPDigitLocation::Default + "2MCHits" ) } },
              KeyValue{ "OutputLocation", Links::location( VPFullClusterLocation::Default + "2MCHits" ) } ) {}

LHCb::LinksByKey LHCb::VP::FullCluster2MCHitLinker::operator()( const std::vector<LHCb::VPFullCluster>& clusters,
                                                                const LHCb::MCHits&                     mcHits,
                                                                const LHCb::LinksByKey& bareDigitLinks ) const {
  // Create an association table between hits and clusters.
  LinksByKey output{ std::in_place_type<VPFullCluster>, std::in_place_type<MCHit> };

  for ( const VPFullCluster& cluster : clusters ) {
    std::map<unsigned int, double> hitMap;
    double                         sum    = 0.;
    const auto&                    pixels = cluster.pixels();
    // Loop over all pixels in a given cluster
    for ( const auto& pixel : pixels ) {
      bareDigitLinks.applyToLinks( pixel, [&hitMap, &sum]( unsigned int, unsigned int tgtIndex, float weight ) {
        hitMap[tgtIndex] += weight;
        sum += weight;
      } );
    }
    if ( sum < 1.e-2 ) continue;
    for ( const auto [mcHitIndex, hitWeight] : hitMap ) {
      output.link( cluster.channelID(), mcHits[mcHitIndex], hitWeight / sum );
    }
  }

  return output;
}

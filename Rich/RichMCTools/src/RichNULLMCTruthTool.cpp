/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichNULLMCTruthTool.cpp
 *
 * Implementation file for class : RichNULLMCTruthTool
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 14/01/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichNULLMCTruthTool.h"

// From PartProp
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// namespace
using namespace Rich::MC;

DECLARE_COMPONENT( NULLMCTruthTool )

const LHCb::MCParticle* NULLMCTruthTool::mcParticle( const LHCb::Track*, const double ) const { return nullptr; }

bool NULLMCTruthTool::mcParticles( const Rich::PDPixelCluster&, std::vector<const LHCb::MCParticle*>& ) const {
  return false;
}

bool NULLMCTruthTool::mcParticles( const LHCb::RichSmartID, std::vector<const LHCb::MCParticle*>& ) const {
  return false;
}

const LHCb::MCRichDigit* NULLMCTruthTool::mcRichDigit( const LHCb::RichSmartID ) const { return nullptr; }

Rich::ParticleIDType NULLMCTruthTool::mcParticleType( const LHCb::MCParticle* ) const { return Rich::Unknown; }

const LHCb::MCRichTrack* NULLMCTruthTool::mcRichTrack( const LHCb::MCParticle* ) const { return nullptr; }

const LHCb::MCRichOpticalPhoton* NULLMCTruthTool::mcOpticalPhoton( const LHCb::MCRichHit* ) const { return nullptr; }

bool NULLMCTruthTool::isBackground( const Rich::PDPixelCluster& ) const { return false; }

bool NULLMCTruthTool::isHPDReflection( const Rich::PDPixelCluster& ) const { return false; }

bool NULLMCTruthTool::isSiBackScatter( const Rich::PDPixelCluster& ) const { return false; }

bool NULLMCTruthTool::isRadScintillation( const Rich::PDPixelCluster& ) const { return false; }

bool NULLMCTruthTool::isBackground( const LHCb::RichSmartID ) const { return false; }

bool NULLMCTruthTool::isHPDReflection( const LHCb::RichSmartID ) const { return false; }

bool NULLMCTruthTool::isSiBackScatter( const LHCb::RichSmartID ) const { return false; }

bool NULLMCTruthTool::isRadScintillation( const LHCb::RichSmartID ) const { return false; }

bool NULLMCTruthTool::isCherenkovRadiation( const Rich::PDPixelCluster&, const Rich::RadiatorType ) const {
  return false;
}

bool NULLMCTruthTool::isCherenkovRadiation( const LHCb::RichSmartID, const Rich::RadiatorType ) const { return false; }

bool NULLMCTruthTool::getMcHistories( const Rich::PDPixelCluster&,
                                      std::vector<const LHCb::MCRichDigitSummary*>& ) const {
  return false;
}

bool NULLMCTruthTool::getMcHistories( const LHCb::RichSmartID, std::vector<const LHCb::MCRichDigitSummary*>& ) const {
  return false;
}

const SmartRefVector<LHCb::MCRichHit>& NULLMCTruthTool::mcRichHits( const LHCb::MCParticle* ) const {
  static const SmartRefVector<LHCb::MCRichHit> tmp;
  return tmp;
}

void NULLMCTruthTool::mcRichHits( const Rich::PDPixelCluster&, SmartRefVector<LHCb::MCRichHit>& ) const {}

const SmartRefVector<LHCb::MCRichHit>& NULLMCTruthTool::mcRichHits( const LHCb::RichSmartID ) const {
  static const SmartRefVector<LHCb::MCRichHit> tmp;
  return tmp;
}

bool NULLMCTruthTool::richMCHistoryAvailable() const { return false; }

bool NULLMCTruthTool::extendedMCAvailable() const { return false; }

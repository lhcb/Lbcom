/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichSmartIDTool.cpp
 *
 * Implementation file for class : Rich::SmartIDTool
 *
 * @author Antonis Papanestis
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2003-10-28
 */
//-----------------------------------------------------------------------------

// local
#include "RichSmartIDTool.h"

#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"

DECLARE_COMPONENT( Rich::SmartIDTool )

StatusCode Rich::SmartIDTool::initialize() {
  return ConditionAccessorHolder::initialize().andThen( [&] {
    addConditionDerivation( { DeRichLocations::OldRich1, DeRichLocations::OldRich2 }, m_photoDetPanels.key(),
                            [&]( DeRich const& r1, DeRich const& r2 ) {
                              RichPDPanels photoDetPanels;
                              for ( DeRich const* r : { &r1, &r2 } ) {
                                photoDetPanels[r->rich()] = PDPanelsPerRich( Rich::NPDPanelsPerRICH, nullptr );
                                for ( unsigned int panel = 0; panel < Rich::NPDPanelsPerRICH; ++panel ) {
                                  photoDetPanels[r->rich()][panel] = r->pdPanel( (Rich::Side)panel );
                                }
                              }
                              return photoDetPanels;
                            } );
    return StatusCode::SUCCESS;
  } );
}

//=============================================================================
// Returns the position of a RichSmartID cluster in global coordinates
// on the PD entrance window
//=============================================================================
bool Rich::SmartIDTool::globalPosition( const Rich::PDPixelCluster& cluster, Gaudi::XYZPoint& detectPoint ) const {
  // Default return status is BAD
  bool sc = false;
  // cluster size
  const auto csize = cluster.size();
  if ( 1 == csize ) {
    // Handle single pixel clusters differently for speed
    sc = globalPosition( cluster.primaryID(), detectPoint );
  } else if ( csize > 1 ) {
    // start with primary ID position
    sc = globalPosition( cluster.primaryID(), detectPoint );
    // secondary IDs
    for ( const auto& S : cluster.secondaryIDs() ) {
      Gaudi::XYZPoint tmpP;
      sc = sc && globalPosition( S, tmpP );
      detectPoint += (Gaudi::XYZVector)tmpP;
    }
    // normalise
    detectPoint /= (double)csize;
  }
  return sc;
}

//=============================================================================
// Returns the position of a RichSmartID in global coordinates
// on the PD entrance window
//=============================================================================
bool Rich::SmartIDTool::globalPosition( const LHCb::RichSmartID& smartID, Gaudi::XYZPoint& detectPoint ) const {
  const auto& richIndex = m_photoDetPanels.get()[smartID.rich()];
  return ( richIndex[smartID.panel()]->detectionPoint( smartID, detectPoint, m_hitPhotoCathSide ) );
}

//=============================================================================
// Returns the global position of a local coordinate, in the given RICH panel
//=============================================================================
Gaudi::XYZPoint Rich::SmartIDTool::globalPosition( const Gaudi::XYZPoint& localPoint, const Rich::DetectorType rich,
                                                   const Rich::Side side ) const {
  const auto& richIndex = m_photoDetPanels.get()[rich];
  return richIndex[side]->PDPanelToGlobalMatrix() * localPoint;
}

//=============================================================================
// Returns the PD position (center of the silicon wafer)
//=============================================================================
bool Rich::SmartIDTool::pdPosition( const LHCb::RichSmartID& pdid, Gaudi::XYZPoint& pdPoint ) const {
  // Create temporary RichSmartIDs for two corners of the PD wafer
  LHCb::RichSmartID id1( pdid ), id0( pdid );
  id0.setPixelRow( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 1 : 10 );
  id0.setPixelCol( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 1 : 10 );
  id1.setPixelRow( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 6 : 21 );
  id1.setPixelCol( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 6 : 21 );

  // get position of each of these pixels
  Gaudi::XYZPoint a, b;
  const auto      sc = globalPosition( id0, a ) && globalPosition( id1, b );

  // return average position (i.e. PD centre)
  pdPoint = ( sc ? Gaudi::XYZPoint( 0.5 * ( a.x() + b.x() ), 0.5 * ( a.y() + b.y() ), 0.5 * ( a.z() + b.z() ) )
                 : Gaudi::XYZPoint( 0, 0, 0 ) );
  return sc;
}

//=============================================================================
// Returns the SmartID for a given global position
//=============================================================================
bool Rich::SmartIDTool::smartID( const Gaudi::XYZPoint& globalPoint, LHCb::RichSmartID& smartid ) const {
  // check to see if the smartID is set, and if PD is active
  if ( smartid.pdIsSet() ) { return false; }

  try {
    if ( globalPoint.z() < 8000.0 ) {
      smartid.setRich( Rich::Rich1 );
      smartid.setPanel( globalPoint.y() > 0.0 ? Rich::top : Rich::bottom );
    } else {
      smartid.setRich( Rich::Rich2 );
      smartid.setPanel( globalPoint.x() > 0.0 ? Rich::left : Rich::right );
    }
    const auto& richIndex = m_photoDetPanels.get()[smartid.rich()];
    return richIndex[smartid.panel()]->smartID( globalPoint, smartid );
  }

  // Catch any GaudiExceptions thrown
  catch ( const GaudiException& excpt ) {
    // Print exception as an error
    Error( "Caught GaudiException " + excpt.tag() + " message '" + excpt.message() + "'" )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    // reset smartid to a default one
    smartid = LHCb::RichSmartID();

    // return failure status
    return false;
  }
}

//=============================================================================
// Converts a point from the global frame to the detector panel frame
//=============================================================================
Gaudi::XYZPoint Rich::SmartIDTool::globalToPDPanel( const Gaudi::XYZPoint& globalPoint ) const {
  if ( globalPoint.z() < 8000.0 ) {
    // Rich1
    const auto& richIndex = m_photoDetPanels.get()[Rich::Rich1];
    if ( globalPoint.y() > 0.0 ) {
      // top side
      return richIndex[Rich::top]->globalToPDPanelMatrix() * globalPoint;
    } else {
      // bottom side
      return richIndex[Rich::bottom]->globalToPDPanelMatrix() * globalPoint;
    }
  } else // Rich2
  {
    const auto& richIndex = m_photoDetPanels.get()[Rich::Rich2];
    if ( globalPoint.x() > 0.0 ) {
      // left side
      return richIndex[Rich::left]->globalToPDPanelMatrix() * globalPoint;
    } else {
      // right side
      return richIndex[Rich::right]->globalToPDPanelMatrix() * globalPoint;
    }
  }
}

//=============================================================================
// Returns a list with all valid smartIDs
//=============================================================================
LHCb::RichSmartID::Vector Rich::SmartIDTool::readoutChannelList( RichPDPanels const& photoDetPanels ) const {
  // the list to return
  LHCb::RichSmartID::Vector readoutChannels;
  // Reserve rough size ( RICH1 + RICH2 )
  readoutChannels.reserve( 400000 );
  bool sc = true;
  // Fill for RICH1
  const auto& rich1Index = photoDetPanels[Rich::Rich1];
  sc                     = sc && rich1Index[Rich::top]->readoutChannelList( readoutChannels );
  sc                     = sc && rich1Index[Rich::bottom]->readoutChannelList( readoutChannels );
  const auto nRich1      = readoutChannels.size();
  // Fill for RICH2
  const auto& rich2Index = photoDetPanels[Rich::Rich2];
  sc                     = sc && rich2Index[Rich::left]->readoutChannelList( readoutChannels );
  sc                     = sc && rich2Index[Rich::right]->readoutChannelList( readoutChannels );
  const auto nRich2      = readoutChannels.size() - nRich1;
  if ( !sc ) Exception( "Problem reading readout channel lists from DeRichPDPanels" );
  // Sort the list
  SmartIDSorter::sortByRegion( readoutChannels );
  info() << "Created active PD channel list : # channels RICH(1/2) = " << nRich1 << " / " << nRich2 << endmsg;
  return readoutChannels;
}

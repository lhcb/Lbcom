/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <array>
#include <cmath>
#include <vector>

// Array properties
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToolHandle.h"

// Utils
#include "RichUtils/RichTrackSegment.h"

// base class
#include "RichFutureKernel/RichToolBase.h"

// interfaces
#include "RichInterfaces/IRichParticleProperties.h"

// boost
#include "boost/limits.hpp"
#include "boost/numeric/conversion/bounds.hpp"

// Kernel
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

namespace Rich::Future {

  //-----------------------------------------------------------------------------
  /** @class ParticleProperties RichParticleProperties.h
   *
   *  Tool to calculate various physical properties
   *  for the different mass hypotheses.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   15/03/2002
   */
  //-----------------------------------------------------------------------------

  class ParticleProperties : public extends<ToolBase, IParticleProperties> {

  public:
    // Methods for Gaudi Framework

    /// Standard constructor
    ParticleProperties( const std::string& type, //
                        const std::string& name, //
                        const IInterface*  parent )
        : extends( type, name, parent ) {
      // initialise
      m_particleMass.fill( 0 );
      m_particleMassSq.fill( 0 );
      // setProperty( "OutputLevel", 1 );
    }

    // Initialize method
    StatusCode initialize() override;

  public:
    // methods (and doxygen comments) inherited from public interface

    // Returns 'beta' for given particle hypothesis
    ScType beta( const ScType ptot, const Rich::ParticleIDType id ) const override {
      const auto Esquare = ptot * ptot + m_particleMassSq[id];
      return ( Rich::BelowThreshold != id && Esquare > 0 ? ptot / std::sqrt( Esquare ) : 0 );
    }

    /// Access the array of all particle masses
    const Rich::ParticleArray<ScType>& masses() const override { return m_particleMass; }

    // Returns the nominal mass for a given particle type
    ScType mass( const Rich::ParticleIDType id ) const override { return m_particleMass[id]; }

    // Returns the nominal mass squared for a given particle type
    ScType massSq( const Rich::ParticleIDType id ) const override { return m_particleMassSq[id]; }

    // Vector of the mass hypotheses to be considered
    const Rich::Particles& particleTypes() const override { return m_pidTypes; }

    // Access the minimum cherenkov photon energies (Aero/R1Gas/R2Gas)
    const DetectorArray<float>& minPhotonEnergy() const override { return m_minPhotEn.value(); }

    // Access the minimum cherenkov photon energies (Aero/R1Gas/R2Gas)
    const DetectorArray<float>& maxPhotonEnergy() const override { return m_maxPhotEn.value(); }

    // Access the minimum cherenkov photon energy for given radiator
    float minPhotonEnergy( const Rich::DetectorType rich ) const override { return m_minPhotEn[rich]; }

    // Access the minimum cherenkov photon energy for given radiator
    float maxPhotonEnergy( const Rich::DetectorType rich ) const override { return m_maxPhotEn[rich]; }

    // Access the mean cherenkov photon energies (Aero/R1Gas/R2Gas)
    float meanPhotonEnergy( const Rich::DetectorType rich ) const override {
      return 0.5f * ( m_maxPhotEn[rich] + m_minPhotEn[rich] );
    }

  private:
    // properties

    /// Particle ID types to consider in the likelihood minimisation (JO)
    Gaudi::Property<std::vector<std::string>> m_pidTypesJO{ this, "ParticleTypes" };

    /// The minimum photon energies for each RICH
    Gaudi::Property<DetectorArray<float>> m_minPhotEn{ this, "MinPhotonEnergy", { 1.75, 1.75 } };

    /// The maximum photon energies for each RICH
    Gaudi::Property<DetectorArray<float>> m_maxPhotEn{ this, "MaxPhotonEnergy", { 7.0, 7.0 } };

  private:
    // Private data

    /// Array containing particle masses
    Rich::ParticleArray<ScType> m_particleMass = { {} };

    /// Array containing square of particle masses
    Rich::ParticleArray<ScType> m_particleMassSq = { {} };

    /// Particle ID types to consider in the likelihood minimisation
    Rich::Particles m_pidTypes{ Rich::particles() };
  };

} // namespace Rich::Future

using namespace Rich::Future;

//-----------------------------------------------------------------------------

DECLARE_COMPONENT( ParticleProperties )

StatusCode ParticleProperties::initialize() {

  // Sets up various tools and services
  auto sc = extends::initialize();
  if ( !sc ) { return sc; }

  // Retrieve particle property service
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  // Retrieve particle masses
  m_particleMass[Rich::Electron]       = ppSvc->find( "e+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Muon]           = ppSvc->find( "mu+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Pion]           = ppSvc->find( "pi+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Kaon]           = ppSvc->find( "K+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Proton]         = ppSvc->find( "p+" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::Deuteron]       = ppSvc->find( "deuteron" )->mass() / Gaudi::Units::MeV;
  m_particleMass[Rich::BelowThreshold] = boost::numeric::bounds<ScType>::highest();

  // cache squares of masses
  m_particleMassSq[Rich::Electron]       = std::pow( m_particleMass[Rich::Electron], 2 );
  m_particleMassSq[Rich::Muon]           = std::pow( m_particleMass[Rich::Muon], 2 );
  m_particleMassSq[Rich::Pion]           = std::pow( m_particleMass[Rich::Pion], 2 );
  m_particleMassSq[Rich::Kaon]           = std::pow( m_particleMass[Rich::Kaon], 2 );
  m_particleMassSq[Rich::Proton]         = std::pow( m_particleMass[Rich::Proton], 2 );
  m_particleMassSq[Rich::Deuteron]       = std::pow( m_particleMass[Rich::Deuteron], 2 );
  m_particleMassSq[Rich::BelowThreshold] = boost::numeric::bounds<ScType>::highest();

  // release service
  if ( sc ) sc = release( ppSvc );

  // PID Types. If JO list is empty (default) full list of PID types is used.
  if ( !m_pidTypesJO.empty() ) {
    m_pidTypes.clear();
    for ( const auto& S : m_pidTypesJO ) {
      if ( "electron" == S ) {
        m_pidTypes.push_back( Rich::Electron );
      } else if ( "muon" == S ) {
        m_pidTypes.push_back( Rich::Muon );
      } else if ( "pion" == S ) {
        m_pidTypes.push_back( Rich::Pion );
      } else if ( "kaon" == S ) {
        m_pidTypes.push_back( Rich::Kaon );
      } else if ( "proton" == S ) {
        m_pidTypes.push_back( Rich::Proton );
      } else if ( "deuteron" == S ) {
        m_pidTypes.push_back( Rich::Deuteron );
      } else if ( "belowThreshold" == S ) {
        m_pidTypes.push_back( Rich::BelowThreshold );
      } else {
        error() << "Unknown particle type from options " << S << endmsg;
        return StatusCode::FAILURE;
      }
    }
  }
  // sort the list to ensure strict (increasing) mass ordering.
  std::sort( m_pidTypes.begin(), m_pidTypes.end() );
  // is pion in the list ?
  const bool hasPion = std::find( m_pidTypes.begin(), m_pidTypes.end(), Rich::Pion ) != m_pidTypes.end();
  info() << "Particle types considered = " << m_pidTypes << endmsg;
  if ( m_pidTypes.empty() ) {
    error() << "No particle types specified" << endmsg;
    return StatusCode::FAILURE;
  }
  if ( !hasPion ) {
    error() << "Pion hypothesis must be enabled" << endmsg;
    return StatusCode::FAILURE;
  }

  // // Informational Printout
  // if ( msgLevel( MSG::DEBUG ) ) {
  //   debug() << " Particle masses (MeV/c^2)     = " << m_particleMass << endmsg;
  //   for ( const auto& pid : m_pidTypes ) {
  //     debug() << pid
  //             << " Momentum Thresholds (GeV/c) :"
  //             //<< " Aero=" << thresholdMomentum(pid,Rich::Aerogel)/Gaudi::Units::GeV
  //             << " R1Gas=" << thresholdMomentum( pid, Rich::Rich1Gas ) / Gaudi::Units::GeV
  //             << " R2Gas=" << thresholdMomentum( pid, Rich::Rich2Gas ) / Gaudi::Units::GeV << endmsg;
  //   }
  // }

  return sc;
}

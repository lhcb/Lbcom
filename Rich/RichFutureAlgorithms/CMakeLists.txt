###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureAlgorithms
-------------------------
#]=======================================================================]

gaudi_add_module(RichFutureAlgorithms
    SOURCES
        src/RichSmartIDClustering.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::DetDescLib
        LHCb::LHCbAlgsLib
        LHCb::RichDetLib
        LHCb::RichFutureKernel
        LHCb::RichFutureUtils
        LHCb::RichUtils
)

# Fixes for GCC7.
if(CMAKE_CXX_COMPILER_ID STREQUAL GNU AND CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "7.0")
    target_compile_options(RichFutureAlgorithms PRIVATE "-faligned-new")
endif()

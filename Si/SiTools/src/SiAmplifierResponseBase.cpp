/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// local
#include "SiAmplifierResponseBase.h"

StatusCode SiAmplifierResponseBase::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  // Fill the ISiAmplifierResponse::Info object. Used to find out for which
  // parameters the spline curve is valid.
  m_info.capacitance = m_capacitance;
  m_info.vfs         = m_vfs;
  m_info.type        = m_type;
  return sc;
}

/// calculate amplifier response
double SiAmplifierResponseBase::response( const double time ) const {
  return ( ( time > m_tMin ) && ( time < m_tMax ) ? m_responseSpline->eval( time ) : 0.0 );
}

/// information on the validity
ISiAmplifierResponse::Info SiAmplifierResponseBase::validity() const { return m_info; }

void SiAmplifierResponseBase::sample( std::vector<double>& times, std::vector<double>& values ) const {

  double t = m_tMin;
  while ( t < m_tMax ) {
    times.push_back( t );
    values.push_back( response( t ) );
    t += m_printDt;
  } // loop times
}

void SiAmplifierResponseBase::printToScreen() const {

  std::vector<double> times;
  std::vector<double> values;
  sample( times, values );
  info() << "Printing response function" << endmsg;
  info() << "Type: " << m_type << endmsg;
  info() << "Vfs: " << m_vfs << endmsg;
  info() << "Assumed Capacitance: " << m_capacitance / Gaudi::Units::picofarad << " pF " << endmsg;
  info() << "risetime [10-90]" << risetime() / Gaudi::Units::ns << endmsg;
  info() << "remainder sampling on peak " << remainder() << endmsg;
  for ( unsigned int i = 0; i < times.size(); ++i ) {
    info() << "Time " << times[i] << " value " << values[i] << endmsg;
  } // loop i
}

void SiAmplifierResponseBase::printForRoot() const {

  std::vector<double> times;
  std::vector<double> values;
  sample( times, values );
  std::cout << "Printing TGraph for " << name() << std::endl;
  std::cout << "void plotGraph(){" << std::endl;
  printArray( times, "times" );
  printArray( values, "values" );
  std::cout << "TGraph* aGraph = new TGraph(" << values.size() << ",times,values);" << std::endl;
  std::cout << "aGraph->Draw(\"APL\");" << std::endl;
  ;
  std::cout << "}" << std::endl;
  ;
}

void SiAmplifierResponseBase::printArray( const std::vector<double>& values, const std::string& name ) const {

  std::cout << "float " << name << "[" << values.size() << "] = {";
  for ( unsigned int i = 0; i < values.size(); ++i ) {
    std::cout << values[i];
    if ( i != values.size() - 1 ) std::cout << ",";
  } // i
  std::cout << "};" << std::endl;
}

double SiAmplifierResponseBase::remainder( double time ) const {
  // find the maximum time
  double tMax = findValue( 1.0 );
  return response( time + tMax + 25.0 * Gaudi::Units::ns );
}

double SiAmplifierResponseBase::findValue( double value ) const {

  // scan across and find the value
  double time = m_tMin;
  do { time += 0.05; } while ( value - response( time ) > 1e-3 );
  return time;
}

double SiAmplifierResponseBase::risetime() const {
  double t_10 = findValue( 0.1 );
  double t_90 = findValue( 0.9 );
  return t_90 - t_10;
}

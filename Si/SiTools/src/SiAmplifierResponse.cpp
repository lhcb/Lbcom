/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiMath/GaudiMath.h"
#include "SiAmplifierResponseBase.h"

/** @class SiAmplifierResponse SiAmplifierResponse.h
 *
 *  Class for estimating Beetle response
 *
 *  @author M.Needham
 *  @date   13/3/2002
 */

class SiAmplifierResponse : public SiAmplifierResponseBase {

public:
  /** Constructer */
  using SiAmplifierResponseBase::SiAmplifierResponseBase;

  /** initialize */
  StatusCode initialize() override;

private:
  Gaudi::Property<std::vector<double>> m_times{ this, "times", {} };   ///< List of times in ns
  Gaudi::Property<std::vector<double>> m_values{ this, "values", {} }; ///< Corresponding values
};

DECLARE_COMPONENT( SiAmplifierResponse )

StatusCode SiAmplifierResponse::initialize() {
  StatusCode sc = SiAmplifierResponseBase::initialize();
  if ( sc.isFailure() ) return sc;

  // Check if the data is provided and the times and values are of equal length
  if ( m_times.empty() ) return Error( "No data !", StatusCode::FAILURE );
  if ( m_times.size() != m_values.size() ) { return Error( "inconsistant data !", StatusCode::FAILURE ); }

  // Store the first and last entry of the vector of times
  m_tMin = m_times.value().front();
  m_tMax = m_times.value().back();

  // Fit the spline to the data
  m_responseSpline = std::make_unique<GaudiMath::SimpleSpline>( m_times, m_values, m_splineType );
  // dump to screen
  if ( m_printToScreen.value() ) printToScreen();
  if ( m_printForRoot.value() ) printForRoot();

  return sc;
}

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SiDepositedChargeBase_H
#define SiDepositedChargeBase_H 1

// Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/SmartIF.h"
#include "GaudiKernel/SystemOfUnits.h"

// Interface
#include "MCInterfaces/ISiDepositedCharge.h"

/** @class SiDepositedChargeBase SiDepositedChargeBase.h
 *
 * Base class for deposited charge tools
 *
 *  @author M.Needham
 *  @date   11/12/2007
 */

class SiDepositedChargeBase : public extends<GaudiTool, ISiDepositedCharge> {

public:
  /** Constructor */
  using extends::extends;

  /** initialize */
  StatusCode initialize() override;

protected:
  /// Calculate the atomic binding effect
  double atomicBinding( const double pathLength ) const;

  // Random number generators
  SmartIF<IRndmGen> m_GaussDist;

  // job options
  Gaudi::Property<double> m_delta2{ this, "delta2",
                                    1800 * Gaudi::Units::keV* Gaudi::Units::keV /
                                        Gaudi::Units::cm }; ///< parameter for the atomic binding effect
  Gaudi::Property<double> m_scalingFactor{ this, "scalingFactor",
                                           1.0 }; ///< Scaling factor for path length through sensor
};

inline double SiDepositedChargeBase::atomicBinding( const double pathLength ) const {
  return sqrt( m_delta2 * pathLength );
}

#endif // SiDepositedChargeBase_H

/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SiAmplifierResponseBase_H
#define SiAmplifierResponseBase_H 1

// Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiMath/GaudiMath.h"

// Interface from LHCbKernel
#include "Kernel/ISiAmplifierResponse.h"

/** @class SiAmplifierResponseBase SiAmplifierResponseBase.h
 *
 *  Base Class for estimating Beetle response
 *
 *  @author M.Needham
 *  @date   9/2/2009
 */

class SiAmplifierResponseBase : public extends<GaudiTool, ISiAmplifierResponse> {

public:
  /** Constructer */
  using extends::extends;

  /** Initialize */
  StatusCode initialize() override;

  /** calculate Beetle response
   * @param time in nanoseconds
   * @return response
   */
  double response( const double time ) const override;

  /** The response is only valid for a certain capacitance, Vfs, etc.
   * This method allows you to find out when the curve is valid
   * @return validity info
   */
  ISiAmplifierResponse::Info validity() const override;

  /** calculate the remainder ie signal left after 25 ns
   * @param time time of sampling on peak [default is zero ]
   * @return response
   */
  double remainder( double time = 0 ) const override;

  /** calculate the rise time [10 - 90 %]
   * @return ristime
   */
  double risetime() const override;

protected:
  /// sample the spline function
  void sample( std::vector<double>& times, std::vector<double>& val ) const;

  /// internal print method
  void printToScreen() const;

  /// print a TGraph for root
  void printForRoot() const;

  /// helper to print array
  void printArray( const std::vector<double>& values, const std::string& name ) const;

  std::unique_ptr<GaudiMath::SimpleSpline> m_responseSpline; ///< The fitted spline

  double m_tMin; ///< First entry in vector of times
  double m_tMax; ///< Last entry in vector of times

  // job options
  Gaudi::Property<GaudiMath::Interpolation::Type> m_splineType{
      this, "splineType", GaudiMath::Interpolation::Type::Cspline }; ///< Spline type (default is "Cspline")
  Gaudi::Property<std::string>  m_type{ this, "type", "signal" };    ///< Info object: Type of data
  Gaudi::Property<unsigned int> m_vfs{ this, "vfs", 400 };           ///< Info object: Signal shaping parameter
  Gaudi::Property<double>       m_capacitance{ this, "capacitance",
                                         18 * Gaudi::Units::picofarad }; ///< Info object: Capacitance
  Gaudi::Property<bool>         m_printToScreen{ this, "printToScreen", false };
  Gaudi::Property<bool>         m_printForRoot{ this, "printForRoot", false };
  Gaudi::Property<double>       m_printDt{ this, "dt", 2.0 * Gaudi::Units::ns };

private:
  double                     findValue( double value ) const;
  ISiAmplifierResponse::Info m_info; ///< Holds information on validity
};

#endif // SiAmplifierResponseBase_H

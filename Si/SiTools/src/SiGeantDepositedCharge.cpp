/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/LHCbConstants.h"
#include "MCInterfaces/ISiDepositedCharge.h"
#include "SiDepositedChargeBase.h"

/** @class SiGeantDepositedCharge SiGeantDepositedCharge.h
 *
 *  Trivial class to turn dE/dx from Geant into e-
 * Allows for scaling + a smearing to simulate atomic binding
 *
 *  @author M.Needham
 *  @date   13/3/2002
 */

class SiGeantDepositedCharge : public extends<SiDepositedChargeBase, ISiDepositedCharge> {

public:
  using extends::extends;

  /** calculate deposited charge (in electrons)
   * @param  aHit hit
   * @return deposited charge
   */
  double charge( const LHCb::MCHit* aHit ) const override;

private:
  Gaudi::Property<bool> m_applySmearing{ this, "applySmearing", false }; /// add atomic binding smearing or not
};

DECLARE_COMPONENT( SiGeantDepositedCharge )

double SiGeantDepositedCharge::charge( const LHCb::MCHit* aHit ) const {
  double aBinding = ( m_applySmearing.value() ? atomicBinding( aHit->pathLength() ) * m_GaussDist->shoot() : 0.0 );
  return m_scalingFactor * ( aHit->energy() + aBinding ) / LHCbConstants::SiEnergyPerIonPair;
}

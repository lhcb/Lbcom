/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiMath/GaudiMath.h"
#include "SiAmplifierResponseBase.h"
#include <utility>

/** @class SiZuriFunction SiZuriFunction.h
 *
 *  Class for estimating Beetle response using second order
 *  CR-RC shaper function. Shape is good up to ~ 30 ns after the peak
 *  Undershoot is overestimated however. This is expected however
 *  since the Beetle has 'active' undershoot cancellation
 *  See: \li <a href = "http://cdsweb.cern.ch/record/837194?ln=en"> LHCb-2005-029 </a>
 *
 *
 *  @author M.Needham
 *  @date   15/1/2009
 */

class SiZuriFunction : public SiAmplifierResponseBase {

public:
  /** Constructer */
  using SiAmplifierResponseBase::SiAmplifierResponseBase;

  /** initialize */
  StatusCode initialize() override;

private:
  /// the actual Zurich function
  double zuriFun( double t ) const;

  std::pair<double, double> findMax() const;

  /// fit par[2] ~ risetime
  Gaudi::Property<double> m_riseTime{ this, "risetime", 15.0 * Gaudi::Units::ns };

  /// range in time
  Gaudi::Property<double> m_tRange{ this, "tRange", 100 * Gaudi::Units::ns };

  /// coupling offset
  Gaudi::Property<double> m_couplingOffset{ this, "m_couplingOffset", 0.0 * Gaudi::Units::ns };

  std::vector<double> m_times;  ///< List of times in ns
  std::vector<double> m_values; ///< Corresponding values
};

DECLARE_COMPONENT( SiZuriFunction )

StatusCode SiZuriFunction::initialize() {
  StatusCode sc = SiAmplifierResponseBase::initialize();
  if ( sc.isFailure() ) return sc;

  // find the maximum....
  m_tMin   = 0.0;
  m_tMax   = m_tRange;
  auto max = findMax();

  // fill the arrays from the zuriFun...
  double t = 0.0;
  while ( t < m_tMax ) {
    m_times.push_back( t - max.first );
    m_values.push_back( zuriFun( t ) / max.second );
    t += m_printDt;
  } // loop times

  // Store the first and last entry of the vector of times
  m_tMin = m_times.front();
  m_tMax = m_times.back();

  // Fit the spline to the data
  if ( m_type == "signal" ) {
    m_responseSpline.reset( new GaudiMath::SimpleSpline( m_times, m_values, m_splineType ) );
  } else {
    // differentiate if cap coupling
    GaudiMath::SimpleSpline tempSpline{ m_times, m_values, m_splineType };
    m_times.clear();
    m_values.clear();
    for ( double t = m_tMin; t < m_tMax; t += m_printDt ) {
      m_times.push_back( t );
      m_values.push_back( tempSpline.deriv( t + m_couplingOffset ) );
    } // loop times and make derivative
    m_responseSpline.reset( new GaudiMath::SimpleSpline( m_times, m_values, m_splineType ) );
  }

  // dump to screen
  if ( m_printToScreen.value() ) printToScreen();
  if ( m_printForRoot.value() ) printForRoot();

  return sc;
}

double SiZuriFunction::zuriFun( const double t ) const {
  return ( t > m_tMin && t < m_tMax )
             ? ( std::pow( t / m_riseTime, 2 ) / 2.0 - std::pow( t / m_riseTime, 3 ) / 6.0 ) * exp( -t / m_riseTime )
             : 0.0;
}

std::pair<double, double> SiZuriFunction::findMax() const {

  // it is this simple
  double tMax = ( 3. - sqrt( 3. ) ) * m_riseTime;
  return { tMax, zuriFun( tMax ) };
}

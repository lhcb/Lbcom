/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCMuonDigit.h"
#include "Event/MCParticle.h"
#include "Event/MuonDigit.h"
#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonDigit2MCParticleAlg
//
// 2005-12-29 : Alessia Satta
//-----------------------------------------------------------------------------

/** @class MuonDigit2MCParticleAlg MuonDigit2MCParticleAlg.h
 *
 *
 *  @author Alessia Satta
 *  @date   2005-12-30
 */
class MuonDigit2MCParticleAlg : public LHCb::Algorithm::Transformer<LHCb::LinksByKey(
                                    LHCb::MuonDigits const&, LHCb::MCParticles const&, LHCb::MCMuonDigits const& )> {
public:
  /// Standard constructor
  MuonDigit2MCParticleAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{
            name,
            pSvcLocator,
            { { "MuonDigitLocation", LHCb::MuonDigitLocation::MuonDigit },
              { "MCParticleLocation", LHCb::MCParticleLocation::Default },
              { "MCMuonDigitLocation", LHCb::MCMuonDigitLocation::MCMuonDigit } },
            { "MuonDigit2MCLinksLocation", LHCb::LinksByKey::linkerName( LHCb::MuonDigitLocation::MuonDigit ) } } {}

  LHCb::LinksByKey operator()( LHCb::MuonDigits const&, LHCb::MCParticles const&,
                               LHCb::MCMuonDigits const& ) const override;

private:
  const LHCb::MCParticle* associateToTruth( const LHCb::MCParticles& mcParticles,
                                            const LHCb::MCMuonDigit& mcDigit ) const;
  Gaudi::Property<bool>   m_associateAll{ this, "AssociateAll", true };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MuonDigit2MCParticleAlg )

//=============================================================================
// Main execution
//=============================================================================
LHCb::LinksByKey MuonDigit2MCParticleAlg::operator()( LHCb::MuonDigits const&   digits,
                                                      LHCb::MCParticles const&  mcParticles,
                                                      LHCb::MCMuonDigits const& mcDigits ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  auto myLink = LHCb::LinksByKey{ std::in_place_type<LHCb::MuonDigit>, std::in_place_type<LHCb::MCParticle>,
                                  LHCb::LinksByKey::Order::decreasingWeight };
  // loop and link MuonDigits to MC truth
  for ( auto& digit : digits ) {
    // match the MCMuonDigit to the MuonDigit via the Key
    const LHCb::MCMuonDigit* mcDigit = mcDigits.object( digit->key() );
    if ( !mcDigit ) {
      error() << "Could not find the match for " << digit->key() << " in " << LHCb::MCMuonDigitLocation::MCMuonDigit
              << endmsg;
      continue;
    }
    if ( auto mcpart = associateToTruth( mcParticles, *mcDigit ); mcpart ) { myLink.link( digit, mcpart ); }
  }
  return myLink;
}

//=============================================================================
const LHCb::MCParticle* MuonDigit2MCParticleAlg::associateToTruth( const LHCb::MCParticles& mcParticles,
                                                                   const LHCb::MCMuonDigit& mcDigit ) const {

  // debug()<<" qui "<<endmsg;
  for ( const LHCb::MCHit* mcHit : mcDigit.mcHits() ) {
    // check the MCMuonHit is still available
    if ( !mcHit ) continue;
    const LHCb::MCParticle* mcPart = mcHit->mcParticle();
    // check found mcParticle
    if ( !mcPart ) continue;
    // check in the current event container
    if ( mcPart->parent() != &mcParticles ) continue;
    // check if muon
    if ( mcPart->particleID().abspid() != 13 ) continue;
    // if muon then ok and exit
    return mcPart;
  }
  // no muon then check the origin of the digit geant hit in current event?
  LHCb::MCMuonDigitInfo digitinfo = mcDigit.DigitInfo();
  if ( digitinfo.isGeantHit() ) {
    if ( digitinfo.doesFiringHitBelongToCurrentEvent() || m_associateAll.value() ) {
      auto itHistory = mcDigit.HitsHistory().begin();
      for ( const LHCb::MCHit* mcHit : mcDigit.mcHits() ) {
        if ( mcHit && itHistory->hasFired() ) {
          if ( const LHCb::MCParticle* mcPart = mcHit->mcParticle(); mcPart ) return mcPart;
        }
        ++itHistory;
      }
    }
  }
  return nullptr;
}
